/* global QUnit */

sap.ui.require(["atriacorps4hanape/mantcl/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
