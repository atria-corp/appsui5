sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("atriacorps4hanape.mantcl.controller.App", {
        onInit: function() {
        }
      });
    }
  );
  