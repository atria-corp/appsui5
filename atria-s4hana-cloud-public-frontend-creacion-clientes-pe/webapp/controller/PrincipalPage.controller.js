sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/m/library",
    "sap/m/Button",
    'sap/m/Bar',
    'sap/m/Title',
    'sap/m/Popover',
    "sap/ui/Device",
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel 
     * @param {typeof sap.m.MessageToast} MessageToast 
     * @param {typeof sap.ui.core.Fragment} Fragment 
     * @param {typeof sap.m.library} library 
     * @param {typeof sap.m.Button} Button 
     * @param {typeof sap.m.Bar} Bar 
     * @param {typeof sap.ui.Device} Device 
     */
    function (Controller, JSONModel, MessageToast, Fragment, library, Button, Bar, Title, Popover, Device) {
        "use strict";
        var sDomain = "";
        var sDestination = "s4hana-extensibility/";
        var sPrefixOdata = "";
        return Controller.extend("atriacorps4hanape.mantcl.controller.PrincipalPage", {
            onInit: function () {
                var oContext = this;

                oContext.byId('dynamicPageId').setShowFooter(true)
                oContext.byId('overFlowTBar').setVisible(true)
                oContext.byId('crearBtn').setEnabled(false)

                sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
                sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
                sDomain = sDomain.length > 0 ? sDomain : "./";

                var oBackButton = new sap.m.Button({
                    icon: "sap-icon://nav-back",
                    visible: false,
                    press: function () {
                        oContext.oMessageView.navigateBack();
                        oContext._oPopover.focus();
                        this.setVisible(false);
                    }
                });

                var oMessageTemplate = new sap.m.MessageItem({
                    type: '{MessageViewModel>type}',
                    title: '{MessageViewModel>title}',
                    description: '{MessageViewModel>description}',
                    subtitle: '{MessageViewModel>subtitle}',
                    counter: '{MessageViewModel>counter}',
                });


                this.oMessageView = new sap.m.MessageView({
                    showDetailsPageHeader: true,
                    itemSelect: function () {
                        oBackButton.setVisible(true);
                    },
                    items: {
                        path: "MessageViewModel>/",
                        template: oMessageTemplate
                    }
                });

                this.oMessageView.setModel(new JSONModel());

                var oCloseButton = new Button({
                    text: "Cerrar",
                    press: function () {
                        oContext._oPopover.close();
                    }
                }).addStyleClass("sapUiTinyMarginEnd"),
                    oPopoverFooter = new Bar({
                        contentRight: oCloseButton
                    }),
                    oPopoverBar = new Bar({
                        contentLeft: [oBackButton],
                        contentMiddle: [
                            new Title({ text: "Log de Transacción" })
                        ]
                    });

                this._oPopover = new Popover({
                    customHeader: oPopoverBar,
                    contentWidth: "440px",
                    contentHeight: "440px",
                    verticalScrolling: false,
                    modal: true,
                    content: [this.oMessageView],
                    footer: oPopoverFooter
                });

                let oView = oContext.getView();

                if (!oView.getModel("Clientes")) {
                    let oProveedorModel = new JSONModel();
                    if (!oProveedorModel.getProperty("/pais")) {
                        oProveedorModel.setProperty("/pais", "PE");
                    }
                    if (!oProveedorModel.getProperty("/lang")) {
                        oProveedorModel.setProperty("/lang", "ES");
                    }
                    if (!oProveedorModel.getProperty("/tipoBP")) {
                        oProveedorModel.setProperty("/tipoBP", []);
                    }
                    oView.setModel(oProveedorModel, "Clientes");
                }
                debugger;
                var deferred = $.Deferred();
                var oDataModel = this.getOwnerComponent().getModel("odata");
                console.log(oDataModel)
                oDataModel.read("/GlobalSettings", {
                    success: function(oData) {
                        console.log(oData)
                        var filteredData = oData.results.filter(function(item) {
                            return item.GlobalSettingType === "ROLES_BUSINESS_PARTNER" && 
                                    item.GlobalSettingClassification === "CHILDREN";
                        });

                        console.log(filteredData);

                        deferred.resolve(filteredData);
                    },
                    error: function(oError) {
                        console.error("Error fetching data:", oError);
                        deferred.reject(oError);
                    }
                });

                deferred.done(function(filteredData) {
                    console.log("Global Settings - Roles",filteredData)
                    if(!oView.getModel("GlobalSettingsBP")){
                        let oRoleModelData = {
                            Roles: filteredData
                        };
                    
                        let oRoleModel = new JSONModel(oRoleModelData);
                        
                        oView.setModel(oRoleModel, "GlobalSettingsBP")
                    }
                });
                
            }, _initBusyIndicator: () => {

                sap.ui.core.BusyIndicator.show(0)

            }, _hideBusyIndicator: () => {

                sap.ui.core.BusyIndicator.hide()

            }, handlePopoverPress: function (oEvent) {
                this.oMessageView.navigateBack();
                this._oPopover.openBy(oEvent.getSource());
                this.byId("mVBtn").setType("Default")
            }, findClientByRUT: async function (oEvent) {
                let oContext = this;
                let selectionSet = oEvent.getParameters().selectionSet;
                let rut = selectionSet[0].getValue();
                let requestResponse = {};
                selectionSet[0].setValue(null)
                if (rut !== "")
                    requestResponse = await oContext.fetchSupplierERPByRUT(rut);
                else {
                    let oProveedorModel = new JSONModel();
                    if (!oProveedorModel.getProperty("/pais")) {
                        oProveedorModel.setProperty("/pais", "PE");
                    }
                    if (!oProveedorModel.getProperty("/lang")) {
                        oProveedorModel.setProperty("/lang", "ES");
                    }
                    if (!oProveedorModel.getProperty("/tipoBP")) {
                        oProveedorModel.setProperty("/tipoBP", []);
                    }
                    this.getView().setModel(oProveedorModel, "Clientes");
                    requestResponse = {
                        success: true,
                        messages: [{
                            title: "Información incompleta",
                            type: "Error",
                            description: `No se ha ingresado un RUC.`,
                            subtitle: "Información no enviada"
                        }],
                        response: {}
                    }
                }
                oContext._showMessageViewModel(requestResponse.messages)
                if (requestResponse.success)
                    oContext.reloadModel();
                console.log(requestResponse)

            }, fetchSupplierERPByRUT: async function (rutCliente) {
                this._initBusyIndicator();
                var oPage = this.byId("dynamicPageId");

                //var endpoint = `https://dev-s4hana-cloud-public-backend-extensibility.cfapps.us10-001.hana.ondemand.com/s4hana-extensibility/integration/s4hana-cloud/v1/business-partner/generic/${rutCliente}`
                var endpoint = `${sDomain + sDestination}integration/s4hana-cloud/v1/business-partner/generic/${rutCliente}`;

                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                }

                let response = {};

                try {
                    let request = await fetch(endpoint, requestOptions)
                    if (request.ok) {
                        console.log(request)
                        let responseBody = await request.json();
                        console.log(responseBody)
                        if (responseBody.response.length !== 0) {
                            this.fetchClientByRUT(rutCliente, true)
                            response = {
                                success: request.ok,
                                messages: [{
                                    title: "Usuario Existente",
                                    type: "Information",
                                    description: `El usuario con RUC ${rutCliente} ya existe en el ERP.`,
                                    subtitle: "Usuario existente en ERP."
                                }],
                                response: {}
                            }
                        } else {
                            response = this.fetchClientByRUT(rutCliente, false)
                        }
                    } else {
                        throw new Error('Network response was not ok.');
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response;
            }, fetchClientByRUT: async function (rutCliente, existing) {
                this._initBusyIndicator();
                //var endpoint = `https://dev-s4hana-cloud-public-backend-extensibility.cfapps.us10-001.hana.ondemand.com/s4hana-extensibility/api/contribuyente/${rutCliente}`
                var endpoint = `${sDomain + sDestination}api/contribuyente/${rutCliente}`;

                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                }

                let response = {};

                try {
                    let request = await fetch(endpoint, requestOptions)
                    if (request.ok) {
                        console.log(request)
                        let responseBody = await request.json();
                        console.log(responseBody)
                        if (responseBody.ruc !== rutCliente) {
                            this.byId('crearBtn').setEnabled(false)
                            let oProveedorModel = new JSONModel();
                            if (!oProveedorModel.getProperty("/pais")) {
                                oProveedorModel.setProperty("/pais", "PE");
                            }
                            if (!oProveedorModel.getProperty("/lang")) {
                                oProveedorModel.setProperty("/lang", "ES");
                            }
                            if (!oProveedorModel.getProperty("/tipoBP")) {
                                oProveedorModel.setProperty("/tipoBP", []);
                            }
                            this.getView().setModel(oProveedorModel, "Clientes");
                            throw new Error('El RUT ingresado no fue encontrado')
                        } else {
                            let message = this.insertData(responseBody, existing)
                            response = {
                                success: true,
                                messages: [{
                                    title: "Operación exitosa",
                                    type: "Success",
                                    description: message,
                                    subtitle: "El usuario ha sido encontrado correctamente."
                                }],
                                response: {}
                            }
                        }
                    } else {
                        throw new Error('Network response was not ok.');
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response;
            }, onRoleMCBSelectionChange: function(oEvent) {
                console.log("Seleccionado ROL",oEvent)
                console.log("Source ROL", oEvent.getSource())
                console.log("Testing ROL",  oEvent.getParameter("changedItems"))
                var aSelectedItems = oEvent.getParameter("changedItems");
                var aRoleValues = [];
            
                if (aSelectedItems) {
                    aSelectedItems.forEach(function(oItem) {
                        var sRoleValue = oItem.getKey();
                        aRoleValues.push(sRoleValue);
                    });
                }
            
                var oProveedorModel = this.getView().getModel("Clientes");
                oProveedorModel.setProperty("/tipoBP", aRoleValues);
            }, onRoleMCBSelectionFinish: function(oEvent) {
                console.log("Seleccionado ROL",oEvent)
                console.log("Source ROL", oEvent.getSource())
                console.log("Testing ROL",  oEvent.getParameter("selectedItems"))
                var aSelectedItems = oEvent.getParameter("selectedItems");
                var aRoleValues = [];
            
                if (aSelectedItems) {
                    aSelectedItems.forEach(function(oItem) {
                        var sRoleValue = oItem.getKey();
                        aRoleValues.push(sRoleValue);
                    });
                }
            
                var oProveedorModel = this.getView().getModel("Clientes");
                oProveedorModel.setProperty("/tipoBP", aRoleValues);
                this.getView().setModel(oProveedorModel, "Clientes");
            }, insertData: function (client, existing) {
                var oComponent = this.getView()
                var oModel = oComponent.getModel("Clientes");
                let pais = oModel.getProperty("/pais")
                let lang = oModel.getProperty("/lang")
                let tipoBP = oModel.getProperty("/tipoBP")
                oModel.setData(client);
                if (!oModel.getProperty("/pais")) {
                    if(pais)
                        oModel.setProperty("/pais", pais);
                    else
                        oModel.setProperty("/pais", "PE");
                }
                if (!oModel.getProperty("/lang")) {
                    if(lang)
                        oModel.setProperty("/lang", lang);
                    else
                        oModel.setProperty("/lang", "ES");
                }
                if (!oModel.getProperty("/tipoBP")) {
                    if(tipoBP)
                        oModel.setProperty("/tipoBP", tipoBP);
                    else
                        oModel.setProperty("/tipoBP", []);
                }
                console.log(oModel.getData())
                this.byId('dynamicPageId').setShowFooter(true)
                this.byId('overFlowTBar').setVisible(true)
                this.byId('crearBtn').setEnabled(!existing)

                oComponent.setModel(oModel, "Clientes");
                this._formFragments = {};

                // Set the initial form to be the display one
                this._showFormFragment("ClientDisplay", existing);
                return "Los datos del cliente han sido cargados."
            }, reloadModel: function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("Clientes")
                if (oModel) {
                    oModel.refresh()
                } else {
                    console.error("Model not found");
                }
            }, saveBPtoERP: async function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("Clientes")
                let oData = oModel.getData()
                console.log("Data", oData)
                let data = {
                    "fullName": oData.razonSocial,
                    "category": "2", //Burned
                    "grouping": "BP02", //Burned
                    "country": oData.pais,
                    "houseNumber": oData.numero,
                    "language": oData.lang, 
                    "postalCode": oData.ubigeo,
                    "streetName": `${oData.tipoVia} ${oData.nombreVia}`,
                    "interior": oData.interior,
                    "kilometer": oData.kilometro,
                    "cityBlock": oData.manzana,
                    "lot": oData.lote,
                    "codeZone": oData.codigoZona,
                    "typeZone": oData.tipoZona,
                    "status": oData.estado,
                    "conditionDom": oData.condicionDomicilio,
                    "taxType": "PE1", //Burned
                    "taxNumber": oData.ruc,
                    "codesTypeBusinessPartner": oData.tipoBP
                }
                let requestResponse = await oContext.fetchSaveBPtoERP(data);
                oContext._showMessageViewModel(requestResponse.messages)
                if (requestResponse.success)
                    oContext.reloadModel();
                console.log(requestResponse)
            }, fetchSaveBPtoERP: async function (data) {
                this._initBusyIndicator();

                console.log("Data to save", data);

                //var endpoint = `https://dev-s4hana-cloud-public-backend-extensibility.cfapps.us10-001.hana.ondemand.com/s4hana-extensibility/masterdata/v1/business-partner/generic/save/`
                var endpoint = `${sDomain + sDestination}masterdata/v1/business-partner/generic/save/`;

                const requestOptions = {
                    method: "POST",
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    redirect: "follow"
                };

                let response = {}

                try {
                    let request = await fetch(endpoint, requestOptions)
                    console.log(request)
                    let test = await request.json()
                    console.log("Testing", test)

                    if (request.ok) {
                        response = {
                            success: request.ok,
                            messages: [{
                                title: "Operación exitosa",
                                type: "Success",
                                description: test.messages[0].description,
                                subtitle: "Creación de BP exitosa"
                            }],
                            response: {}
                        }
                        this.byId('crearBtn').setEnabled(false)
                        this.getView().setModel(new JSONModel(), "Clientes");
                    } else {
                        throw new Error("Error al realizar la petición. " + test.error);
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }

                } finally {
                    this._hideBusyIndicator()
                }
                return response
            }, _showMessageViewModel: function (messages) {
                var oContext = this;

                var oBackButton = new sap.m.Button({
                    icon: "sap-icon://nav-back",
                    visible: false,
                    press: function () {
                        oContext.oMessageView.navigateBack();
                        oContext._oPopover.focus();
                        this.setVisible(false);
                    }
                });

                var oMessageTemplate = new sap.m.MessageItem({
                    type: '{MessageViewModel>type}',
                    title: '{MessageViewModel>title}',
                    description: '{MessageViewModel>description}',
                    subtitle: '{MessageViewModel>subtitle}',
                    counter: '{MessageViewModel>counter}',
                });


                this.oMessageView = new sap.m.MessageView({
                    showDetailsPageHeader: false,
                    itemSelect: function () {
                        oBackButton.setVisible(true);
                    },
                    items: {
                        path: "MessageViewModel>/",
                        template: oMessageTemplate
                    }
                });

                var oCloseButton = new Button({
                    text: "Cerrar",
                    press: function () {
                        oContext._oPopover.close();
                    }
                }).addStyleClass("sapUiTinyMarginEnd"),
                    oPopoverFooter = new Bar({
                        contentRight: oCloseButton
                    }),
                    oPopoverBar = new Bar({
                        contentLeft: [oBackButton],
                        contentMiddle: [
                            new Title({ text: "Log de Transacción" })
                        ]
                    });

                this._oPopover = new Popover({
                    customHeader: oPopoverBar,
                    contentWidth: "440px",
                    contentHeight: "440px",
                    verticalScrolling: false,
                    modal: true,
                    content: [this.oMessageView],
                    footer: oPopoverFooter
                });

                var oModel = new JSONModel();

                oModel.setData(messages);

                this.oMessageView.setModel(oModel, "MessageViewModel");

                this.oMessageView.navigateBack();
                console.log("Button Messages", messages)
                if (messages) {
                    if (messages[0].type === 'Error')
                        this.byId("mVBtn").setType("Critical");
                    else if (messages[0].type === 'Success')
                        this.byId("mVBtn").setType("Success");
                    else if (messages[0].type === 'Information')
                        this.byId("mVBtn").setType("Neutral")
                } else
                    this.byId("mVBtn").setType("Critical")
            },

            _toggleButtonsAndView: function (bEdit) {
                var oView = this.getView();

                // Show the appropriate action buttons
                oView.byId("edit").setVisible(!bEdit);
                oView.byId("save").setVisible(bEdit);
                oView.byId("cancel").setVisible(bEdit);

                // Set the right form type
                this._showFormFragment(bEdit ? "ClientChange" : "ClientDisplay");
            },

            _getFormFragment: function (sFragmentName) {
                var pFormFragment = this._formFragments[sFragmentName],
                    oView = this.getView();

                if (!pFormFragment) {
                    pFormFragment = Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hanape.mantcl.view.fragments." + sFragmentName
                    });
                    this._formFragments[sFragmentName] = pFormFragment;
                }

                return pFormFragment;
            },

            _showFormFragment: function (sFragmentName, existing) {
                var oPage = this.byId("dynamicPageId");

                // Destroy existing content
                oPage.destroyContent();

                // Load the fragment
                this._getFormFragment(sFragmentName).then(function (oVBox) {
                    // Check if the DynamicPage is still valid
                    if (!oPage.bIsDestroyed) {
                        // Set the fragment as content of the DynamicPage
                        oPage.setContent(oVBox);
                        console.log(oVBox.getItems())
                        var oSecondForm = oVBox.getItems()[1];
                        console.log(oSecondForm.getContent())
                        if (oSecondForm) {
                            var aSelects = oSecondForm.getContent().filter(function (oItem) {
                                return oItem instanceof sap.m.Select;
                            });

                            aSelects.forEach(function (oSelect) {
                                oSelect.setEditable(!existing);
                            });
                        } else {
                            console.error("Second form not found.");
                        }
                        oPage.bIsDestroyed = null;
                    } else {
                        // Handle the case where the DynamicPage has been destroyed
                        console.error("DynamicPage has been destroyed, cannot set content.");
                    }
                }).catch(function (error) {
                    // Handle errors loading the fragment
                    console.error("Error loading form fragment:", error);
                });
            }
        });
    });
