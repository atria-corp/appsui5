sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "atriacorps4hanach/monitordoccompras/model/models",
    "sap/f/library",
    "sap/f/FlexibleColumnLayoutSemanticHelper",
    "sap/base/util/UriParameters",
    "sap/ui/model/json/JSONModel"
],
    function (UIComponent, Device, models, library, FlexibleColumnLayoutSemanticHelper, UriParameters, JSONModel) {
        "use strict";

        var LayoutType = library.LayoutType;

        return UIComponent.extend("atriacorps4hanach.monitordoccompras.Component", {
            metadata: {
                manifest: "json",
                config: {
                    fullWidth: true
                }
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                var oModel = new JSONModel();

                let oModelConfig = new JSONModel({
                    views: ['Listado', "Reporte"],
                    globalCurrentView: 'Listado'
                })

                this.setModel(models.createDeviceModel(), "device");
                this.setModel(oModelConfig, "customConfigurationModel")
                this.setModel(oModel)

                this.getRouter().initialize();

            },

            getHelper: function () {

                var oFCL = this.getRootControl().byId("fcl"),
                    oParams = UriParameters.fromQuery(location.search),
                    oSettings = {
                        defaultTwoColumnLayoutType: LayoutType.TwoColumnsMidExpanded,
                        defaultThreeColumnLayoutType: LayoutType.ThreeColumnsMidExpanded,
                        mode: oParams.get("mode"),
                        maxColumnsCount: oParams.get("max")
                    };

                return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
            }

        });
    }
);