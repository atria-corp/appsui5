/* global QUnit */

sap.ui.require(["atriacorps4hanach/monitordoccompras/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
