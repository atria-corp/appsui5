/* global QUnit */

sap.ui.require(["atriacorps4hanach/calcinteres/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
