## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Fri May 24 2024 16:17:13 GMT+0000 (Coordinated Universal Time)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>1.13.5|
|**Generation Platform**<br>SAP Business Application Studio|
|**Template Used**<br>simple|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>https://s4hana-cloud-public-backend-extensibility.cfapps.us10-001.hana.ondemand.com/s4hana-extensibility/odata.svc/
|**Module Name**<br>calcinteres|
|**Application Title**<br> Aplicación para el cálculo de intereses|
|**Namespace**<br>atriacorps4hanach|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.124.0|
|**Enable Code Assist Libraries**<br>False|
|**Enable TypeScript**<br>False|
|**Add Eslint configuration**<br>False|

## calcinteres

 Aplicación para el cálculo de intereses Chile

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

- It is also possible to run the application using mock data that reflects the OData Service URL supplied during application generation.  In order to run the application with Mock Data, run the following from the generated app root folder:

```
    npm run start-mock
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


