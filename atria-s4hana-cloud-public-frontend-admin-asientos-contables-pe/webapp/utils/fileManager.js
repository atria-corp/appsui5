sap.ui.define(
    [
      "../lib/exceljs",
      "../lib/FileSaver",
      // "../lib/xlsx"
    ],
    (
      exceljs,
      FileSaver
      //  xlsx
    ) => {
      "use strict";
  
      return {
        /**
         * Downloads a given Blob object as a file.
         * @param {Blob} blob - The Blob object to be downloaded.
         * @param {string} filename - The name of the file to be downloaded.
         * @example
         * const blob = new Blob(['Hello, world!'], { type: 'text/plain' });
         * this.downloadBlob(blob, 'hello.txt');
         */
        downloadBlob: async function (blob, filename) {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement("a");
          a.href = url;
          a.download = filename;
          document.body.appendChild(a);
          a.click();
          a.remove();
          window.URL.revokeObjectURL(url);
        },
  
        /**
         * Generates an Excel file from the given data and triggers a download.
         * @param {Object[]} data - The data to be included in the Excel file.
         * @param {string} fileName - The name of the generated Excel file.
         * @example
         * const data = [
         *   { "Name": "John Doe", "Age": 30 },
         *   { "Name": "Jane Doe", "Age": 25 }
         * ];
         * this.generateExcel(data, 'report');
         */
        generateExcel: function (data, fileName) {
          const workbook = new ExcelJS.Workbook();
          const worksheet = workbook.addWorksheet("REPORTE");
  
          // Add headers
          const headers = Object.keys(data[0]);
          worksheet.addRow(headers);
  
          // Style headers
          headers.forEach((header, index) => {
            const cell = worksheet.getCell(1, index + 1);
            cell.font = { bold: true, size: 13, color: { argb: "FFFFFF" } };
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "BF0909" },
            };
            cell.alignment = { vertical: "middle", horizontal: "center" };
          });
  
          // Add data
          data.forEach((item) => {
            const row = headers.map((header) => item[header]);
            worksheet.addRow(row);
          });
  
          // Auto-adjust column widths
          worksheet.columns.forEach((column) => {
            let maxLength = 0;
            column.eachCell({ includeEmpty: true }, (cell) => {
              const columnLength = cell.value ? cell.value.toString().length : 10;
              if (cell.type === ExcelJS.ValueType.Date) {
                maxLength = 20;
              } else if (columnLength > maxLength) {
                maxLength = columnLength + 3;
              }
            });
            column.width = maxLength < 10 ? 10 : maxLength;
          });
  
          // Save the workbook to a blob
          workbook.xlsx.writeBuffer().then((buffer) => {
            const blob = new Blob([buffer], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            });
            saveAs(blob, `${fileName}.xlsx`);
          });
        },
  
        /**
         * Generates an Excel file from the given data and triggers a download.
         * @param {Object[]} data - The data to be included in the Excel file.
         * @param {string} fileName - The name of the generated Excel file.
         * @example
         * const data = [
         *   { "Name": "John Doe", "Age": 30 },
         *   { "Name": "Jane Doe", "Age": 25 }
         * ];
         * this.generateXlsx(data, 'report');
         */
        /*
        generateXlsx: function (data, fileName) {
          let row = [
            {
              v: "Courier: 24",
              t: "s",
              s: { font: { name: "Courier", sz: 24 } },
            },
            {
              v: "bold & color",
              t: "s",
              s: { font: { bold: true, color: { rgb: "FF0000" } } },
            },
            {
              v: "fill: color",
              t: "s",
              s: { fill: { fgColor: { rgb: "E9E9E9" } } },
            },
            { v: "line\nbreak", t: "s", s: { alignment: { wrapText: true } } },
          ];
  
          const worksheet = XLSX.utils.json_to_sheet(data);
          const workbook = XLSX.utils.book_new();
          const ws = XLSX.utils.aoa_to_sheet([row]);
          XLSX.utils.book_append_sheet(workbook, worksheet, "Dates");
          XLSX.utils.sheet_add_aoa(worksheet, [["Name"]], { origin: "A1" });
  
          const max_width = data.reduce((w, r) => Math.max(w, r.name.length), 10);
          worksheet["!cols"] = [{ wch: max_width }];
  
          XLSX.writeFile(workbook, `${fileName}.xlsx`, { compression: true });
        },
        */
      };
    }
  );
  