sap.ui.define(["sap/ui/model/json/JSONModel",
  "sap/m/MessageToast",
  "sap/m/MessageBox",
  "sap/ui/core/Fragment",
  "sap/m/library",
  "sap/m/Button",
  "sap/ui/Device",
  "sap/ushell/Container",
  "sap/ui/core/format/DateFormat",
  "sap/ui/core/date/UI5Date",
  "sap/ui/model/Filter",
  "sap/ui/model/FilterOperator",
  "sap/tnt/InfoLabel",
  "sap/suite/ui/commons/MicroProcessFlow",
  "sap/suite/ui/commons/MicroProcessFlowItem"], (
    JSONModel,
    MessageToast,
    MessageBox,
    Fragment,
    library,
    Button,
    Device,
    Container,
    DateFormat,
    UI5Date,
    Filter,
    FilterOperator,
    InfoLabel) => {
  "use strict";

  return {
    uploadFile: async (oEvt) => {
      var that = sap.ui.controller("atriacorps4hanape.monitordocventas.controller.Detail")._getController();
      console.log(that)
      console.log("Prueba ")
      console.log(oEvt.getSource().getParent().getBindingContext("odata").getObject())
      var {Id} = oEvt.getSource().getParent().getBindingContext("odata").getObject()
      that._initBusyIndicator();
      let oContext = that;
      var oFiles = oEvt.getParameter("files");
      var oFile = oFiles[0];
      const formdata = new FormData();
      formdata.append("file", oFile, oFile.name);
      formdata.append("Id", Id)

      var oSourceId = oEvt.getSource().getId();

      var response = await that.loadFile.uploadZip(formdata, oSourceId);

      oContext._showMessageViewModel(response.messages);
      if (response.success) {
        oContext.reloadModel();
        MessageToast.show("Documento cargado con éxito.");
      }

      //oContext._openDialogByName("PreviewData");
      //response.
      oEvt.getSource().setValue(null);
      //console.log(response);
      that._hideBusyIndicator();
      sap.ui.controller("atriacorps4hanape.monitordocventas.controller.Detail").onRefreshReferences();
    },
    onShowBtnUpload: (oEvt)=> {
      console.log("----->"+oEvt)
      if((oEvt * 1) > 0){
        return false
      }else{
        return true
      }
    },
    onEnabledUrl: (oEvt)=> {
      console.log("----->"+oEvt)
      if((oEvt * 1) > 0){
        return true
      }else{
        return false
      }
    },
    uploadZip: async (formdata, oSourceId) => {
      var { sDomain, sDestination } = sap.ui.controller("atriacorps4hanape.monitordocventas.controller.Detail")._getPathRest();
      var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/uploadAttachmentByReference`;

      const requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };

      let response = {};

      try {
        let request = await fetch(endpoint, requestOptions);
        let responseData = await request.json(); // Assuming the response is in JSON format
        response = responseData;
        console.log(response);
      } catch (error) {
        console.log(error);
      } finally {
      }
      return response;
    },
  };
});