sap.ui.define([], () => {
    "use strict";
  
    return {
      /**
       * Retrieves selected items from a given table and returns them as an array of objects.
       * Supports both sap.m.Table and sap.ui.table.Table.
       * @param {sap.m.Table | sap.ui.table.Table} oTable - The table from which to retrieve selected items.
       * @returns {Promise<object[]>} - A promise that resolves with an array of selected items as objects.
       * @example
       * const selectedItems = await this.getSelectedItemsObject(oTable);
       * console.log(selectedItems);
       */
      getSelectedItemsObject: async function (oTable) {
        let aSelectedItems = [];
  
        if (oTable instanceof sap.m.Table) {
          aSelectedItems = oTable.getSelectedItems();
        } else if (oTable instanceof sap.ui.table.Table) {
          aSelectedItems = oTable
            .getSelectedIndices()
            .map((index) => oTable.getContextByIndex(index).getObject());
        } else {
          // throw new Error(`Unsupported table type`);
          console.error("Unsupported table type");
          return [];
        }
  
        return aSelectedItems;
      },
    };
  });
  