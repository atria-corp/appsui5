sap.ui.define([], () => {
    "use strict";
  
    return {
      /**
       * Constructs a full ID from a given prefix ID and suffix.
       * This method splits the prefix ID by "--", replaces the last part with the suffix,
       * and then joins the parts back together.
       * @param {string} prefixId - The initial prefix ID to be modified.
       * @param {string} suffix - The suffix to replace the last part of the prefix ID.
       * @returns {string} - The constructed full ID.
       * @example
       * const fullId = this.getFullId("prefix--part1--part2", "newPart");
       * console.log(fullId); // Outputs: "prefix--part1--newPart"
       */
      getFullId: function (prefixId, suffix) {
        return prefixId.split("--").slice(0, -1).concat(suffix).join("--");
      },
  
      /**
       * Checks if a given content is an empty object.
       * An object is considered empty if it has no own enumerable properties.
       * @param {object} content - The content to be checked.
       * @returns {boolean} - True if the content is an empty object, false otherwise.
       * @example
       * const isEmpty = this.isEmptyObj({});
       * console.log(isEmpty); // Outputs: true
       */
      isEmptyObj: function (content) {
        return !(
          content &&
          typeof content === "object" &&
          Object.keys(content).length > 0
        );
      },
  
      /**
       * Checks if a given content is an empty array.
       * An array is considered empty if it has no elements.
       * @param {any} content - The content to be checked.
       * @returns {boolean} - True if the content is an empty array, false otherwise.
       * @example
       * const isEmpty = this.isEmptyArray([]);
       * console.log(isEmpty); // Outputs: true
       */
      isEmptyArray: function (content) {
        return !Array.isArray(content) || content.length === 0;
      },
  
      /**
       * Formats a URL by appending encoded query parameters to a base URL.
       * Each query parameter is URL-encoded to ensure it is safe for use in a URL.
       * Single quotes are specifically encoded as %27 to prevent issues in URL parsing.
       *
       * @param {string} baseUrl - The base URL to which the query parameters will be appended.
       * @param {string} queryParams - A string of query parameters in the format "key1=value1&key2=value2".
       * @returns {string} - The formatted URL with encoded query parameters.
       * @example
       * const url = this.formatUrl("https://example.com", "name=John Doe&age=30");
       * console.log(url); // Outputs: "https://example.com?name=John%20Doe&age=30"
       */
      formatUrl: function (baseUrl, queryParams) {
        const params = queryParams
          .split("&")
          .map((param) => {
            const [key, value] = param.split("=");
            return `${key}=${encodeURIComponent(value).replace(/'/g, "%27")}`;
          })
          .join("&");
  
        return `${baseUrl}?${params}`;
      },
  
      /**
       * Generates a unique code based on the current timestamp.
       * The timestamp is converted to a base-36 string and returned in uppercase.
       * This method provides a simple way to generate a unique identifier.
       *
       * @returns {string} - A unique code based on the current timestamp, represented as an uppercase base-36 string.
       * @example
       * const uniqueCode = this.getUniqueCode();
       * console.log(uniqueCode); // Outputs a unique code like "KDG9Q"
       */
      getUniqueCode: function () {
        const now = new Date();
        const timestamp = now.getTime();
        const uniqueCode = timestamp.toString(36); // Convertir a base 36 para reducir longitud
        return uniqueCode.toUpperCase();
      },
    };
  });
  