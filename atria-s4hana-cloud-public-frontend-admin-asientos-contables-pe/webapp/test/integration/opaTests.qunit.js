/* global QUnit */

sap.ui.require(["atriacorps4hanape/admasientocon/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
