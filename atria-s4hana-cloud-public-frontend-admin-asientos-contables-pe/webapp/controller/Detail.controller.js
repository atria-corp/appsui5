sap.ui.define(
  [
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/core/format/DateFormat",
    "sap/m/MessageToast",
    "../model/formatter",
    "../utils/loadFile",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/m/Button",
    "../utils/util",
    "../utils/fileManager",
    "../utils/utilElements",
  ],
  function (JSONModel, Controller, MessageBox, DateFormat, MessageToast, formatter, loadFile, Filter, FilterOperator, Button, util, fileManager, utilElements) {
    "use strict";
    //
    var globalCurrentDocument;
    var contextGlobal;
    var sDomain = "";
    var sDestination = "s4hana-extensibility/";
    var sPrefixOdata = "";
    var iNumero = 0;
    var oView;
    return Controller.extend(
      "atriacorps4hanape.admasientocon.controller.Detail",
      {
        formatter: formatter,
        loadFile: loadFile,
        util: util,
        fileManager: fileManager,
        utilElements: utilElements,

        _getController: function () {
          return contextGlobal;
        },
        _getPathRest: function () {
          return {
            sDomain,
            sDestination
          };
        },
        onRefreshReferences: () => {
          let referenceFilter = new Filter({
            path: "DocumentoCompraBotId",
            operator: FilterOperator.EQ,
            value1: iNumero,
          });

          // oView.byId("referenceTable").bindRows({
          //   path: `odata>/DocumentoCompraBotReferencias`,
          //   parameters: {
          //     "expand": "ManagementAttachmentDetails"
          //   },
          //   filters: [referenceFilter]
          // });
          oView.byId("referenceTable").bindRows({
            path: `odata>/DocumentoVentaPerus(${iNumero})/DocumentoVentaPeruReferenciaDetails`,
            parameters: {
              "expand": "ManagementAttachmentDetails"
            },
            filters: [],
          });
        },
        onInit: async function () {
          contextGlobal = this;
          oView = this.getView();
          // contextGlobal._initBusyIndicator();
          var oExitButton = this.getView().byId("exitFullScreenBtn");
          var oEnterButton = this.getView().byId("enterFullScreenBtn");

          sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
          sDomain = this.getOwnerComponent()
            .getModel("odata")
            .sServiceUrl.split("s4hana-extensibility")[0];
          sDomain = sDomain.length > 0 ? sDomain : "./";

          this.oRouter = this.getOwnerComponent().getRouter();
          this.oModel = this.getOwnerComponent().getModel();
          //console.log(this.getView())
          this.oRouter
            .getRoute("detail")
            .attachPatternMatched(this._bindingDocumentMatcher, this);

          [oExitButton, oEnterButton].forEach(function (oButton) {
            oButton.addEventDelegate({
              onAfterRendering: function () {
                if (this.bFocusFullScreenButton) {
                  this.bFocusFullScreenButton = false;
                  oButton.focus();
                  // contextGlobal._hideBusyIndicator();
                }
              }.bind(this),
            });
          }, this);
        },

        handleFullScreen: function () {
          this.bFocusFullScreenButton = true;
          var sNextLayout = this.oModel.getProperty(
            "/actionButtonsInfo/midColumn/fullScreen",
          );
          this.oRouter.navTo("detail", {
            layout: sNextLayout,
            supplier: globalCurrentDocument.Id,
          });
        },

        handleExitFullScreen: function () {
          this.bFocusFullScreenButton = true;
          var sNextLayout = this.oModel.getProperty(
            "/actionButtonsInfo/midColumn/exitFullScreen",
          );
          this.oRouter.navTo("detail", {
            layout: sNextLayout,
            supplier: globalCurrentDocument.Id,
          });
        },

        handleClose: function () {
          var sNextLayout = this.oModel.getProperty(
            "/actionButtonsInfo/midColumn/closeColumn",
          );
          this.oRouter.navTo("list", { layout: sNextLayout });
        },

        _bindingDocumentMatcher: async function (oEvent) {
          //--- Obtenemos la view
          debugger;
          let oView = this.getView();

          //--- Obtenemos la posición dentro del array del proveedor seleccionado
          const iNumDoc = oEvent.getParameter("arguments").supplier || 0;

          iNumero = iNumDoc;
          const oData = oView.getModel("odata").oData;

          // if(iNumDoc){
          //   console.log(" Detail ====>"+iNumDoc)
          //   sap.ui.controller("atriacorps4hanape.admasientocon.controller.App").onShowTwoColumn(iNumDoc);
          // }
          // Vincular tablas
          oView.byId("detailsTable").bindRows({
            path: `odata>/DocumentoVentaPerus(${iNumDoc})/DocumentoVentaPeruDetalleDetails`,
            filters: [],
          });
          oView.byId("referenceTable").bindRows({
            path: `odata>/DocumentoVentaPerus(${iNumDoc})/DocumentoVentaPeruReferenciaDetails`,
            parameters: {
              "expand": "ManagementAttachmentDetails"
            },
            filters: [],
          });
          oView.byId("logTable").bindRows({
            path: `odata>/DocumentoVentaPerus(${iNumDoc})/DocumentoVentaPeruLogDetails`,
            filters: [],
          });

          try {
            // Obtener datos del documento
            var oDocument = await this._fetchDocument(iNumDoc);
            var oDateFormat = DateFormat.getDateTimeInstance({
              pattern: "dd/MM/yyyy",
            });

            // Formatear fechas del documento
            oDocument.d.DocumentoFechaEmision = this._formatDate(
              oDocument.d.DocumentoFechaEmision,
              oDateFormat,
            );
            oDocument.d.DocumentoFechaVencimiento = this._formatDate(
              oDocument.d.DocumentoFechaVencimiento,
              oDateFormat,
            );

            // Establecer modelo "documentData"
            oView.setModel(new JSONModel(oDocument.d), "documentData");
            globalCurrentDocument = oDocument.d;

            // Obtener y establecer logs del documento si existe la función
            if (this._fetchDocumentLogs) {
              var documentLogs = await this._fetchDocumentLogs(iNumDoc);
              oView.setModel(
                new JSONModel(documentLogs.d.results),
                "documentsLogs",
              );
            }
          } catch (error) {
            console.error("Error al obtener los datos del documento:", error);
          }
        },

        _formatDate: function (oDateValue, oDateFormat) {
          if (!oDateValue) {
            return "";
          }
          var timestamp = parseInt(
            oDateValue.replace("/Date(", "").replace(")/", ""),
            10,
          );
          var oDate = new Date(timestamp);
          return oDateFormat.format(oDate);
        },

        _fetchDocumentDetails: async (sapId) => {
          contextGlobal._initBusyIndicator();

          var endpoint =
            "/s4hana-extensibility/odata.svc/DetalleVentaSigges?$format=json&$filter=NumeroDocumento eq '" +
            sapId +
            "'";
          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });

          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            //console.log("TestingResult", responseJson)
            return responseJson;
          } catch (ex) {
            console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd")
            contextGlobal._hideBusyIndicator();
          }
        },

        _fetchDocumentReference: async (sapId) => {
          contextGlobal._initBusyIndicator();
          var endpoint =
            "/s4hana-extensibility/odata.svc/Referencias?$format=json&$filter=NumeroDocumento eq '" +
            sapId +
            "'";
          let responseJson;

          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });

          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions);
            responseJson = await request.json();
            //console.log("TestingResult", responseJson)
            return responseJson;
          } catch (ex) {
            console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd")
            contextGlobal._hideBusyIndicator();
          }
        },

        _fetchDocumentLogs: async (sapId) => {
          contextGlobal._initBusyIndicator();
          //console.log("TestingInside")
          //var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
          var endpoint = `${sDomain + sDestination}odata.svc/DocumentoVentaPerus(${sapId})/DocumentoVentaPeruLogDetails?$format=json`;

          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });

          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            //console.log("TestingResult", responseJson)
            //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
            return responseJson;
          } catch (ex) {
            console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd")
            contextGlobal._hideBusyIndicator();
          }
        },

        _fetchDocument: async (sapId) => {
          contextGlobal._initBusyIndicator();
          //console.log("TestingInside")
          //var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
          var endpoint = `${sDomain + sDestination}odata.svc/ViewDocumentoVentaPerus(${sapId})?$format=json`;

          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });

          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            //console.log("TestingResult", responseJson)
            //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
            return responseJson;
          } catch (ex) {
            console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd")
            contextGlobal._hideBusyIndicator();
          }
        },

        _buildProcessFlowInput: (detailList) => {
          let position = 0;
          let listSize = detailList.length;
          //console.log("TestingSize",listSize)
          let lanes = [];
          let nodes = [];

          detailList.forEach((item) => {
            //console.log("TestingItem",item)
          });
          return { lanes: lanes, nodes: nodes };
        },

        _initBusyIndicator: () => {
          sap.ui.core.BusyIndicator.show();
        },

        _hideBusyIndicator: () => {
          sap.ui.core.BusyIndicator.hide();
        },
        onGenerateSharedLink: async (oEvt) => {
          console.log(oEvt.getSource().getParent().getBindingContext("odata").getObject())
          //console.log(oEvt.getSource().getParent().getBindingContext("odata").getProperty("/ManagementAttachmentDetails/AttachmentFileIdSp"))
          var { DocumentoCodigo, FileName, AttachmentId } = oEvt.getSource().getParent().getBindingContext("odata").getObject();
          contextGlobal._initBusyIndicator();
          var sUrl = `purchase/v1/purchase-document-bot/getSharedLinkv2/${AttachmentId}`;
          var endpoint = `${sDomain + sDestination}${sUrl}`
          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });
          var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions)
            //console.log(request)
            let { url, message } = await request.json();
            if (message)
              MessageBox.error(message)
            else
              sap.m.URLHelper.redirect(url, true);
          } catch (ex) {
            //console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd")
            contextGlobal._hideBusyIndicator()
          }
        },
        _showMessageViewModel: function (messages) {
          var oContext = this;

          var oBackButton = new sap.m.Button({
            icon: "sap-icon://nav-back",
            visible: false,
            press: function () {
              oContext.oMessageView.navigateBack();
              this.setVisible(true);
            },
          });

          var oMessageTemplate = new sap.m.MessageItem({
            type: "{MessageViewModel>type}",
            title: "{MessageViewModel>title}",
            description: "{MessageViewModel>description}",
            subtitle: "{MessageViewModel>subtitle}",
            counter: "{MessageViewModel>counter}",
          });

          this.oMessageView = new sap.m.MessageView({
            showDetailsPageHeader: true,
            itemSelect: function () {
              oBackButton.setVisible(true);
            },
            items: {
              path: "MessageViewModel>/",
              template: oMessageTemplate,
            },
          });

          this.oDialog = new sap.m.Dialog({
            title: "Log de Transacción",
            resizable: true,
            content: this.oMessageView,
            beginButton: new sap.m.Button({
              press: function () {
                this.getParent().close();
              },
              text: "Close",
            }),
            contentHeight: "50%",
            contentWidth: "50%",
            verticalScrolling: false,
          });

          var oModel = new JSONModel();

          oModel.setData(messages);

          this.oMessageView.setModel(oModel, "MessageViewModel");

          this.oMessageView.navigateBack();

          this.oDialog.open();
        },

        sendToErp: async function () {
          debugger;

          var docId = this.getView().getModel("documentData").getProperty("/Id");

          let requestResponse = await this.fetchValidateAttachments(docId);

          this._showMessageViewModelAttachment(requestResponse.messages);

          var arr = $.grep(requestResponse.messages, function (n, i) {
            return n.group == "SUCCESS";
          });
          arr = $.map(arr, (value) => {
            return value.subtitle;
          });
          console.log(arr);
          this.aFilesSendToErp = arr;
          return;
        },

        onShowConfirmation: function (aSelectedItems) {
          var oContext = this;
          let idArray = [];
          if (aSelectedItems.length > 0) {
            let message = `¿Está seguro que desea enviar ${aSelectedItems.length} registro(s)?`;
            let title = "Confirmación";
            let actions = ["Si", "No"];
            let onPress = async function (option) {
              switch (option) {
                case "Si":
                  aSelectedItems.forEach(function (item) {
                    idArray.push(item);
                  });
                  //let requestResponse = await oContext.fetchSendToErp(idArray);
                  let requestResponse = await oContext.fetchSendToErp(idArray);
                  oContext._showMessageViewModel(requestResponse.messages);
                  console.log(requestResponse.messages);
                  oContext.reloadModel();
                  break;
              }
            };
            sap.m.MessageBox.confirm(message, {
              title,
              actions,
              emphasizedAction: sap.m.MessageBox.Action.YES,
              onClose: onPress,
            });
          } else MessageToast.show("Seleccione los documentos");
        },

        fetchSendToErp: async function (idArray) {
          this._initBusyIndicator();
          var oContext = this;
          //var endpoint = `${sDomain + sDestination}sales/v1/sales-document/rejected-documents`;
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          //console.log(endpoint)
          let response = {};

          const requestBody = idArray;

          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            let { messages } = responseJson;
            ////console.log("TestingDelete", responseJson)
            if (request.status === 200) {
              response = {
                success: true,
                messages,
                response: {},
              };
            } else if (request.status === 500) {
              response = {
                success: false,
                messages,
                response: {},
              };
            }
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },

        _showMessageViewModelAttachment: function (messages) {
          var oBackButton = new sap.m.Button({
            icon: "sap-icon://nav-back",
            visible: false,
            press: function () {
              this.oMessageView.navigateBack();
              this.setVisible(true);
            },
          });

          var oMessageTemplate = new sap.m.MessageItem({
            type: "{MessageViewModelAttachment>type}",
            title: "{MessageViewModelAttachment>title}",
            description: "{MessageViewModelAttachment>description}",
            subtitle: "{MessageViewModelAttachment>subtitle}",
            counter: "{MessageViewModelAttachment>counter}",
            groupName: "{MessageViewModelAttachment>group}",
          });

          this.oMessageView = new sap.m.MessageView({
            showDetailsPageHeader: true,
            itemSelect: function () {
              oBackButton.setVisible(true);
            },
            items: {
              path: "MessageViewModelAttachment>/",
              template: oMessageTemplate,
            },
            groupItems: true,
          });

          this.oDialog = new sap.m.Dialog({
            title: "Validacion de adjuntos",
            resizable: true,
            content: this.oMessageView,
            beginButton: new Button({
              press: function () {
                this.getParent().close();
              },
              text: "Close",
            }),
            endButton: new Button({
              press: (oEvt) => {
                oEvt.getSource().getParent().close();
                //console.log(this.aFilesSendToErp);
                if (this.aFilesSendToErp.length < 1) {
                  return MessageBox.warning(
                    "No hay documentos validos para enviar a SAP.",
                  );
                }
                this.onShowConfirmation(this.aFilesSendToErp);
              },
              text: "Enviar documento validos.",
            }),
            contentHeight: "50%",
            contentWidth: "50%",
            verticalScrolling: false,
          });

          var oModel = new JSONModel();

          oModel.setData(messages);

          this.oMessageView.setModel(oModel, "MessageViewModelAttachment");

          this.oMessageView.navigateBack();

          this.oDialog.open();
        },

        fetchValidateAttachments: async function (docId) {
          this._initBusyIndicator();
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/validateAttachment`;
          let response = {};
          const requestBody = [docId];
          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            response = responseJson;
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },

        //Generar Reporte

        onPressExportDataTSPPA: async function () {
          let that = this;
          that._initBusyIndicator();

          try {
            let oTable = that.getView().byId("detailsTable");
            let oBinding = oTable.getBinding("rows");

            if (oBinding) {
              // Obtener los contextos del binding
              let aContexts = oBinding.getContexts();
              // Mapear los contextos a los objetos de datos
              let aContent = aContexts.map((oContext) => oContext.getObject());

              if (!util.isEmptyArray(aContent)) {
                let aData = aContent.map((o) => that._buildDataReportTSPPA(o));
                fileManager.generateExcel(aData, `Reporte del Detalle del Documento N° ${globalCurrentDocument.NumeroFactura}`);
              } else {
                MessageToast.show("No se encontraron registros para generar el reporte.");
              }
            } else {
              console.error("El binding de la tabla no está definido.");
            }
          } catch (oError) {
            console.log(oError);
          } finally {
            that._hideBusyIndicator();
          }
        },

        _buildDataReportTSPPA: function (o) {
          return {
            "N° de Documento": o.NumeroDocumentoOrigen,
            "N° Linea": o.NroLinea,
            "Código Material": o.MaterialCodigo,
            Descripcion: o.Descripcion,
            "Contrato de Venta": o.NroContratoVenta,
            Cantidad: o.Cantidad,
            "Contrato de Compra": o.NroContratoCompra,
            "Precio Unitario": o.PrecioUnitario,
            "Sub Total": o.SubTotal,
            Total: o.Total,
            "Aplicacion de nota crédito": o.AplicacionNotaCredito,
            "Material Agrupador": o.MaterialAgrupador,
          };
        },
      },
    );
  },
);
