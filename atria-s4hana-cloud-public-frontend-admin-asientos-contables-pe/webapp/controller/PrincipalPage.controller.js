sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/m/library",
    "sap/m/Button",
    "sap/ui/Device",
    "sap/ushell/Container",
    "sap/ui/core/format/DateFormat",
    "sap/ui/core/date/UI5Date",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/tnt/InfoLabel",
    "../model/formatter",
    "sap/suite/ui/commons/MicroProcessFlow",
    "sap/suite/ui/commons/MicroProcessFlowItem",
    "../utils/util",
    "../utils/fileManager",
    "../utils/utilElements",
  ],
  /**
   * @param {typeof sap.ui.core.mvc.Controller} Controller
   */
  function (
    Controller, JSONModel, MessageToast, Fragment, library, Button, Device, Container, DateFormat, UI5Date, Filter, FilterOperator, InfoLabel, formatter, MicroProcessFlow, MicroProcessFlowItem, util, fileManager, utilElements) {
    "use strict";

    var URLHelper = library.URLHelper;
    var oComponent = null;
    var sDomain = "";
    var sDestination = "s4hana-extensibility/";
    var sPrefixOdata = "";
    var oView;
    var that;
    var aMessagesToShow = [];
    var aIds = [];
    return Controller.extend(
      "atriacorps4hanape.admasientocon.controller.PrincipalPage",
      {
        formatter: formatter,
        util: util,
        fileManager: fileManager,
        utilElements: utilElements,
        onInit: function () {
          oComponent = this.getOwnerComponent();

          sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
          sDomain = this.getOwnerComponent()
            .getModel("odata")
            .sServiceUrl.split("s4hana-extensibility")[0];
          sDomain = sDomain.length > 0 ? sDomain : "./";

          this.oRouter = this.getOwnerComponent().getRouter();

          this._applicationsSwitchData();

          this._initApplicationPopover();

          this._initConfigurationDialogs();

          this._defineCustomModel();

          oView = this.getView();
          //var oJSONModel = this.initSampleDataModel();
          //console.log(oJSONModel)
          //oView.setModel(this.initSampleDataModel(), "tmp");

          oView.setModel(
            new JSONModel({
              globalFilter: "",
              availabilityFilterOn: false,
              cellFilterOn: false,
            }),
            "ui",
          );

          this._oGlobalFilter = null;
          this._oPriceFilter = null;
          that = this;
        },
        _filter: function () {
          var oFilter = null;

          if (this._oGlobalFilter && this._oPriceFilter) {
            oFilter = new Filter(
              [this._oGlobalFilter, this._oPriceFilter],
              true,
            );
          } else if (this._oGlobalFilter) {
            oFilter = this._oGlobalFilter;
          } else if (this._oPriceFilter) {
            oFilter = this._oPriceFilter;
          }

          this.byId("table").getBinding().filter(oFilter, "Application");
        },

        filterGlobally: function (oEvent) {
          var sQuery = oEvent.getParameter("query");
          this._oGlobalFilter = null;

          if (sQuery) {
            this._oGlobalFilter = new Filter(
              [
                new Filter("Name", FilterOperator.Contains, sQuery),
                new Filter("Category", FilterOperator.Contains, sQuery),
              ],
              false,
            );
          }

          this._filter();
        },

        filterPrice: function (oEvent) {
          var oColumn = oEvent.getParameter("column");
          if (oColumn != this.byId("price")) {
            return;
          }

          oEvent.preventDefault();

          var sValue = oEvent.getParameter("value");

          function clear() {
            this._oPriceFilter = null;
            oColumn.setFiltered(false);
            this._filter();
          }

          if (!sValue) {
            clear.apply(this);
            return;
          }

          var fValue = null;
          try {
            fValue = parseFloat(sValue, 10);
          } catch (e) {
            // nothing
          }

          if (!isNaN(fValue)) {
            this._oPriceFilter = new Filter(
              "Price",
              FilterOperator.BT,
              fValue - 20,
              fValue + 20,
            );
            oColumn.setFiltered(true);
            this._filter();
          } else {
            clear.apply(this);
          }
        },

        clearAllFilters: function (oEvent) {
          var oTable = this.byId("table");

          var oUiModel = this.getView().getModel("ui");
          oUiModel.setProperty("/globalFilter", "");
          oUiModel.setProperty("/availabilityFilterOn", false);

          this._oGlobalFilter = null;
          this._oPriceFilter = null;
          this._filter();

          var aColumns = oTable.getColumns();
          for (var i = 0; i < aColumns.length; i++) {
            oTable.filter(aColumns[i], null);
          }
        },
        initSampleDataModel: function () {
          var oModel = new JSONModel();

          var oDateFormat = DateFormat.getDateInstance({
            source: { pattern: "timestamp" },
            pattern: "dd/MM/yyyy",
          });

          jQuery.ajax(
            "https://sapui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/products.json",
            {
              dataType: "json",
              success: function (oData) {
                var aTemp1 = [];
                var aTemp2 = [];
                var aSuppliersData = [];
                var aCategoryData = [];
                for (var i = 0; i < oData.ProductCollection.length; i++) {
                  var oProduct = oData.ProductCollection[i];
                  if (
                    oProduct.SupplierName &&
                    aTemp1.indexOf(oProduct.SupplierName) < 0
                  ) {
                    aTemp1.push(oProduct.SupplierName);
                    aSuppliersData.push({ Name: oProduct.SupplierName });
                  }
                  if (
                    oProduct.Category &&
                    aTemp2.indexOf(oProduct.Category) < 0
                  ) {
                    aTemp2.push(oProduct.Category);
                    aCategoryData.push({ Name: oProduct.Category });
                  }
                  oProduct.DeliveryDate =
                    Date.now() - (i % 10) * 4 * 24 * 60 * 60 * 1000;
                  oProduct.DeliveryDateStr = oDateFormat.format(
                    UI5Date.getInstance(oProduct.DeliveryDate),
                  );
                  oProduct.Heavy =
                    oProduct.WeightMeasure > 1000 ? "true" : "false";
                  oProduct.Available =
                    oProduct.Status == "Available" ? true : false;
                }

                oData.Suppliers = aSuppliersData;
                oData.Categories = aCategoryData;

                oModel.setData(oData);
              },
              error: function () {
                Log.error("failed to load json");
              },
            },
          );

          return oModel;
        },
        onInitialiseTable: function () {
          var smarTableComponent = this.getView().byId("tbl-documentos");

          var tableColumns = smarTableComponent.getTable().getColumns();

          tableColumns.forEach((oColumn) => {
            var oContext = this._configureTableColumn(
              oColumn.getFilterProperty(),
            );

            oColumn.setHAlign(oContext.hAlign);
            oColumn.setWidth(oContext.width);
            oColumn.getLabel().setTextAlign("Center");
            oColumn.getLabel().setWrapping(true);
            oColumn.getLabel().setWrappingType("Hyphenated");
            oColumn.mAggregations.customData[0].mProperties.value.type =
              oContext.type;
            oColumn.mAggregations.customData[0].mProperties.value.width = null;
            oColumn.setTemplate(oContext.control);
          });
        },

        _defineCustomModel: function () {
          debugger;

          let oView = this.getView();

          let oTable = oView.byId("tbl-documentos");
          let oFilter = oView.byId("smartFilterBar");
          let oDataModel = this.getOwnerComponent().getModel("odata");

          oTable.setModel(oDataModel);
          //oView.byId("LineItemsSmartTable").setModel(oDataModel)
          oFilter.setModel(oDataModel);
        },

        _configureTableColumn: function (columnName) {
          var context = this;

          var oColumnsProperties = [
            {
              hAlign: "Center",
              width: "8rem",
              name: "DocumentoCodigo",
              control: new sap.m.Link({
                text: "{= ${DocumentoCodigo} }",
              }).attachPress(function (evt) {
                context.onListItemPress(evt);
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "NumeroDocumentoOrigen",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${NumeroDocumentoOrigen} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "CodigoCliente",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${CodigoCliente} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "10rem",
              name: "StatusName",
              type: "string",
              control: new sap.tnt.InfoLabel({
                text: {
                  path: "StatusName",
                },
                icon: {
                  path: "StatusId",
                  formatter: function (estadoValue) {
                    switch (estadoValue) {
                      case 1:
                        return "sap-icon://present";
                      case 2:
                        return "sap-icon://message-success";
                      case 3:
                        return "sap-icon://message-success";
                      case 4:
                        return "sap-icon://status-error";
                      case 5:
                        return "sap-icon://cloud";
                      case 6:
                        return "sap-icon://message-success";
                      case 7:
                        return "sap-icon://message-success";
                      case 8:
                        return "sap-icon://status-error";
                      case 9:
                        return "sap-icon://status-error";
                      default:
                        return "sap-icon://status-error";
                    }
                  },
                },
                colorScheme: {
                  path: "StatusId",
                  formatter: function (estadoValue) {
                    switch (estadoValue) {
                      case 1:
                        return 10;
                      case 2:
                        return 8;
                      case 3:
                        return 2;
                      case 4:
                        return 2;
                      case 5:
                        return 6;
                      case 6:
                        return 8;
                      case 7:
                        return 8;
                      case 8:
                        return 3;
                      case 9:
                        return 2;
                      default:
                        return 3;
                    }
                  },
                },
                width: "8rem",
                wrapping: false,
                textAlign: sap.ui.core.TextAlign.Center,
                style:
                  "max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;",
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "TipoDocumento",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${TipoDocumento} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "6rem",
              name: "CondicionPago",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${CondicionPago} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "10rem",
              name: "ErpCreditMemoReq",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${ErpCreditMemoReq} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "TextoRegistro",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${TextoRegistro} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "ErpBusinessSolutionOrder",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${ErpBusinessSolutionOrder} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "Id",
              control: new sap.m.VBox({
                items: [
                  new sap.suite.ui.commons.MicroProcessFlow({
                    content: [
                      //--- Recepcionado o Eliminado
                      new sap.suite.ui.commons.MicroProcessFlowItem({
                        icon: "sap-icon://create-form",
                        state: "{= ${StatusId} === 9 ? 'Error' : 'Success' }",
                        title: "Recepcionado o Eliminado",
                      }),
                      //--- Creado en SAP
                      new sap.suite.ui.commons.MicroProcessFlowItem({
                        state:
                          "{= ${ErpBusinessSolutionOrder} === null ? ${ErpCreditMemoReq} === null ? 'None' : 'Success' : 'Success'}",
                        icon: "sap-icon://order-status",
                        title: "Creado en SAP",
                      }),
                    ],
                  }),
                ],
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "MesServicio",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${MesServicio} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "MonedaTransaccion",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${MonedaTransaccion} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "12rem",
              name: "NumeroFacturaRelacionada",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${NumeroFacturaRelacionada} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "NumeroSuministro",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${NumeroSuministro} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "Origen",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${Origen} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "SubTotal",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${SubTotal} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "Total",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${Total} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "DocumentoFechaRegistro",
              type: "string",
              control: new sap.m.Text({
                //text: moment("{= ${Consideraciones} }").utc().format("YYYY-MM-DD"),
                text: "{path: 'DocumentoFechaRegistro', type: 'sap.ui.model.type.Date', formatOptions: {UTC: true, pattern: 'dd/MM/yyyy'}}",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "15rem",
              name: "Consideraciones",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${Consideraciones} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "TipoNotaCredito",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${TipoNotaCredito} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "TipoServicio",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${TipoServicio} }",
                wrapping: false,
              }),
            },
            {
              hAlign: "Center",
              width: "8rem",
              name: "NroSerie",
              type: "string",
              control: new sap.m.Text({
                text: "{= ${NroSerie} }",
                wrapping: false,
              })
            },
          ];

          var aColumn = $.grep(oColumnsProperties, function (item) {
            if (columnName == item.name) return item;
          });

          return aColumn.length > 0 ? aColumn[0] : {};
        },

        onPressVisualizarListado: function () {
          var view = this.getView();
          var model = view.getModel("customConfigurationModel");
          var data = model.getData();

          data.globalCurrentView = "Listado";

          model.setData(data);
        },
        onPressVisualizarResumen: function () {
          var view = this.getView();
          var model = view.getModel("customConfigurationModel");
          var data = model.getData();

          data.globalCurrentView = "Reporte";

          model.setData(data);
        },

        _initBusyIndicator: () => {
          sap.ui.core.BusyIndicator.show(0);
        },

        _hideBusyIndicator: () => {
          sap.ui.core.BusyIndicator.hide();
        },

        onListItemPress: function (oEvent) {
          debugger;
          var oNextUIState = this.getOwnerComponent()
            .getHelper()
            .getNextUIState(1);
          var currentDocument = oEvent
            .getSource()
            .getParent()
            .getRowBindingContext()
            .getProperty();
          //console.log(currentDocument)

          this.oRouter.navTo("detail", {
            layout: oNextUIState.layout,
            supplier: currentDocument.Id,
          });
          //this.oRouter.navTo("detail", { layout: oNextUIState.layout, supplier: currentDocument.GlobalSettingClassification })
          // this._initBusyIndicator();
        },
        onForcedShowDetail: function (oParam) {
          debugger;
          var oNextUIState = that.getOwnerComponent()
            .getHelper()
            .getNextUIState(1);
          //console.log(currentDocument)

          that.oRouter.navTo("detail", {
            layout: oNextUIState.layout,
            supplier: oParam,
          });
          //this.oRouter.navTo("detail", { layout: oNextUIState.layout, supplier: currentDocument.GlobalSettingClassification })
          // this._initBusyIndicator();
        },

        downloadExcelTemplate: async function () {
          //this._initBusyIndicator()
          //var endpoint = "/s4hana-extensibility/sales/v1/sales-document/download-template"
          ///https://s4hana-cloud-public-backend-extensibility.cfapps.us10-001.hana.ondemand.com/s4hana-extensibility/sales/v1/sales-document/cargaMasivaDocumentoVenta/xlsx/template
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/cargaMasivaDocumentoVentaPeru/xlsx/template`;
          let response = {};
          URLHelper.redirect(endpoint, true);
          //
          return;
          const requestOptions = {
            method: "GET",
            redirect: "follow",
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            if (request.ok) {
              let blob = await request.blob();
              const url = window.URL.createObjectURL(blob);
              const a = document.createElement("a");
              a.href = url;
              a.download = "Plantilla - Doc Ventas.xlsx"; // You can specify the filename here
              a.style.display = "none"; // Hide the element
              document.body.appendChild(a);
              a.click();
              window.URL.revokeObjectURL(url);
            } else {
              throw new Error("Network response was not ok.");
            }
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },

        readDataExcel: async function (oEvent) {
          //alert("Llamar al servicio para la lectura del excel")
          //oView.setModel(new JSONModel([]), "ModelImport");
          let oContext = this;
          //console.log(oEvent)
          var oFiles = oEvent.getParameter("files");
          //console.log(oFiles)

          var oFile = oFiles[0];
          //console.log(oFile)

          const formdata = new FormData();
          formdata.append("file", oFile, oFile.name);
          var response = await this.fetchUploadExcel(formdata);

          if (response.success) oContext.reloadModel();

          oContext._openDialogByName(
            "PreviewData",
            "ModelImport",
            response.responseBody,
          );
          oEvent.getSource().setValue(null);
          ////console.log(response)
        },
        fetchUploadExcel: async function (formdata) {
          //var endpoint = "s4hana-extensibility/sales/v1/sales-document/upload-massive-data";
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/cargaMasivaDocumentoVentaPeru/xlsx/file`;

          const requestOptions = {
            method: "POST",
            body: formdata,
            redirect: "follow",
          };

          let response = {};

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseBody = await request.json();
            //console.log(request)
            //console.log()

            response = {
              success: request.ok,
              messages: [
                {
                  title: "Operación exitosa",
                  type: "Success",
                  description:
                    "Se cargaron correctamente los documentos de venta",
                  subtitle: "Carga de documentos exitosa",
                },
              ],
              responseBody,
            };
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            //this._hideBusyIndicator()
          }
          return response;
        },
        onUploadZip: async function (oEvent) {
          debugger;
          this._initBusyIndicator();
          let oContext = this;
          var oFiles = oEvent.getParameter("files");
          var oFile = oFiles[0];
          const formdata = new FormData();
          formdata.append("file", oFile, oFile.name);

          var oSourceId = oEvent.getSource().getId();

          var response = await this.uploadZip(formdata, oSourceId);

          oContext._showMessageViewModel(response.messages);
          if (response.success) {
            oContext.reloadModel();
            MessageToast.show("Documento cargado con éxito.");
          }

          //oContext._openDialogByName("PreviewData");
          //response.
          oEvent.getSource().setValue(null);
          //console.log(response);
          this._hideBusyIndicator();
        },
        uploadZip: async function (formdata, oSourceId) {
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/uploadAttachment/v1/123`;

          const requestOptions = {
            method: "POST",
            body: formdata,
            redirect: "follow",
          };

          let response = {};

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseData = await request.json(); // Assuming the response is in JSON format
            response = responseData;
            console.log(response);
          } catch (error) {
            console.log(error);
          } finally {
          }
          return response;
        },
        deleteDocuments: async function (oEvent) {
          //alert("Llamar al servicio para la eliminación de información")
          var oContext = this;

          var oSmartTable = oContext.getView().byId("tbl-documentos");
          //console.log(oSmartTable)
          var oTable = oSmartTable.getTable();
          //console.log(oTable)
          var aSelectedItems = [];
          if (oTable instanceof sap.m.Table) {
            aSelectedItems = oTable.getSelectedItems();
          } else if (oTable instanceof sap.ui.table.Table) {
            aSelectedItems = oTable.getSelectedIndices().map(function (index) {
              return oTable.getContextByIndex(index).getObject();
            });
          } else {
            // Handle other types of tables
            console.error("Unsupported table type");
          }
          let idArray = [];
          if (aSelectedItems.length > 0) {
            //console.log("Selected " + aSelectedItems.length + " document(s)", aSelectedItems)
            let message = `¿Está seguro que desea eliminar ${aSelectedItems.length} registro(s)?`;
            let title = "Confirmación";
            let actions = ["Si", "No"];
            let onPress = async function (option) {
              switch (option) {
                case "Si":
                  aSelectedItems.forEach(function (item) {
                    //console.log(item);
                    //let idUrl = item.__metadata.Id;
                    //console.log(idUrl)
                    //let sapId = idUrl.match(/'([^']+)'/)[1];
                    //console.log(sapId)
                    idArray.push(item.Id);
                  });
                  //console.log("TestingArray", idArray)
                  let requestResponse =
                    await oContext.fetchDeleteDocument(idArray);
                  oContext._showMessageViewModel(requestResponse.messages);
                  //console.log(requestResponse.messages)

                  oContext.reloadModel();
                  break;
              }
            };

            sap.m.MessageBox.confirm(message, {
              title,
              actions,
              emphasizedAction: sap.m.MessageBox.Action.YES,
              onClose: onPress,
            });
          } else MessageToast.show("Seleccione los documentos");
        },

        fetchDeleteDocument: async function (idArray) {
          this._initBusyIndicator();
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/rejected-documents-peru`;
          //var endpoint = `${sDomain + sDestination}sales/v1/sales-document/create-sales-document-in-sap`;
          //console.log(endpoint)
          let response = {};

          const requestBody = idArray;

          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            //console.log("TestingDelete", responseJson)
            let { messages } = responseJson;
            if (request.status === 200) {
              response = {
                success: true,
                messages,
                response: {},
              };
            } else if (request.status === 500) {
              response = {
                success: false,
                messages,
                response: {},
              };
            }
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },
        fetchSendToErpv2: async function (idArray) {
          this._initBusyIndicator();
          var oContext = this;
          var sURl = "";
          //var endpoint = `${sDomain + sDestination}sales/v1/sales-document/rejected-documents`;
          // if (idArray[0] == 3024 || idArray[0] == 3016 || idArray[0] == 3012) {
          //   sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap-1`;
          // } else {
          //   sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          // }
          sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          //console.log(endpoint)
          let response = {};

          const requestBody = idArray;

          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };
          return fetch(sURl, requestOptions).then(res => res.json())
          //let responseJson = await request.json();

          //return response;
        },
        fetchSendToErp: async function (idArray) {
          this._initBusyIndicator();
          var oContext = this;
          //var endpoint = `${sDomain + sDestination}sales/v1/sales-document/rejected-documents`;
          if (idArray[0] == 3024 || idArray[0] == 3016 || idArray[0] == 3012) {
            sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap-1`;
          } else {
            sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          }
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          //console.log(endpoint)
          let response = {};

          const requestBody = idArray;

          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            let { messages } = responseJson;
            ////console.log("TestingDelete", responseJson)
            if (request.status === 200) {
              response = {
                success: true,
                messages,
                response: {},
              };
            } else if (request.status === 500) {
              response = {
                success: false,
                messages,
                response: {},
              };
            }
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },

        reloadModel: function () {
          let oContext = this;
          let oModel = oContext.getView().getModel("odata");
          if (oModel) {
            oModel.refresh();
            oModel.read("/ViewDocumentoVentas", {
              success: function () {
                // Data read successfully, no action required
              },
              error: function () {
                console.error("Failed to reload model");
              },
            });
          } else {
            console.error("Model not found");
          }
        },
        onShowConfirmation: (aSelectedItems) => {
          aMessagesToShow = [];
          aIds = [];
          var oContext = that;
          let idArray = [];
          if (aSelectedItems.length > 0) {
            let message = `¿Está seguro que desea enviar ${aSelectedItems.length} registro(s)?`;
            let title = "Confirmación";
            let actions = ["Si", "No"];
            let onPress = async function (option) {
              switch (option) {
                case "Si":
                  var promises = [];
                  that._initBusyIndicator();
                  $.each(aSelectedItems, (key, value) => {
                    aIds.push({id:value,fire:false});
                  });
                  that.onGetResultFetch();
                  console.log(aIds);
                  break;
              }
            };
            sap.m.MessageBox.confirm(message, {
              title,
              actions,
              emphasizedAction: sap.m.MessageBox.Action.YES,
              onClose: onPress,
            });
          } else MessageToast.show("Seleccione los documentos");
        },
        onGetResultFetch: async () => {
          var promises = [];
          var iCount = 0;
          var aTmp = $.grep(aIds, function( n, i ) {
            //console.log(n.)
            return ( n.fire == false );
          });
          console.log(aTmp.length)
          aIds = aTmp;
          if(aTmp.length < 10){
            $.each(aIds, (key, value) => {
            
              if(!value.fire){
                promises.push(that.fetchSendToErpv2([value.id]));
                value.fire = true;
                //aIds.splice(0, 1);
                iCount = iCount + 1;
                //if(iCount == 10){
                  //that.generateData(promises)
                  //console.log(promises)
                //  promises = [];
                 // iCount = 0;
                 // return false;
                //}
              }
            });
            that.generateData(promises, true)
          }else{
            $.each(aIds, (key, value) => {
            
              if(!value.fire){
                promises.push(that.fetchSendToErpv2([value.id]));
                value.fire = true;
                //aIds.splice(0, 1);
                iCount = iCount + 1;
                if(iCount == 10){
                  that.generateData(promises)
                  //console.log(promises)
                  promises = [];
                  iCount = 0;
                  return false;
                }
              }
              
            });
          }
        },
        generateData: async (promises, show) => {
          const data = await Promise.all(promises)
          $.each(data, (key, value) => {
            if (value.hasOwnProperty("messages")) {
              aMessagesToShow = [...aMessagesToShow, ...value.messages];
            } else if (value.hasOwnProperty("error")) {
              aMessagesToShow.push({
                "type": "Error",
                "title": value.error,
                "description": value.path,
                "subtitle": value.error,
                "group": value.status,
              });
            }
          })
          console.log(aMessagesToShow)
          console.log(aIds)
          
          if(show){
            that._hideBusyIndicator();
            that._showMessageViewModel(aMessagesToShow);
          }else{
            that.onGetResultFetch();
          }
          //that._showMessageViewModel(aMessagesToShow);
        },
        generateRequest: (iId) => {
          var oTmp = [];
          var sURl = ""
          //if(iId == 3024 || iId == 3016 || iId == 3012){
          // sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap-1`;
          // }else{
          sURl = `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`;
          //}
          oTmp.push(iId);
          return new Promise((resolve, reject) => {
            $.ajax({
              context: that,
              //url: `${sDomain + sDestination}sales/v1/sales-document-peru/create-sales-document-peru-in-sap`,
              url: sURl,
              type: "POST",
              data: JSON.stringify(oTmp),
              contentType: "application/json",
              dataType: "json",
              success: resolve,
              //error: reject,
              complete: resolve

            });
          })
        },
        fetchValidateAttachments: async function (idArray) {
          this._initBusyIndicator();
          var oContext = this;
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/validateAttachment`;
          let response = {};
          const requestBody = idArray;
          var requestOptions = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
          };

          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            response = responseJson;
          } catch (error) {
            response = {
              success: false,
              messages: [
                {
                  title: "Ocurrió un problema",
                  type: "Error",
                  description: `${error}`,
                  subtitle: "Comuníquese con el administrador del sistema.",
                },
              ],
              response: {},
            };
          } finally {
            this._hideBusyIndicator();
          }
          return response;
        },
        sendToErp: async function (oEvent) {
          //alert( "Llamar al servicio ODATA para la creación de pedido de venta" )
          var oContext = this;
          var oSmartTable = this.getView().byId("tbl-documentos");
          //console.log(oSmartTable)
          var oTable = oSmartTable.getTable();
          //console.log(oTable)
          var aSelectedItems = [];
          var aFilterDocs = [];
          if (oTable instanceof sap.m.Table) {
            aSelectedItems = oTable.getSelectedItems();
          } else if (oTable instanceof sap.ui.table.Table) {
            aSelectedItems = oTable.getSelectedIndices().map(function (index) {
              try {
                console.log(index)
                aFilterDocs.push(oTable.getContextByIndex(index).getObject().Id)
                return oTable.getContextByIndex(index).getObject();
              } catch (error) {
                console.log(error, index)
              }
              //aFilterDocs.push(oTable.getContextByIndex(index).getObject().Id)
              
            });
          } else {
            //console.error("Unsupported table type");
          }
          //console.log(aFilterDocs);
          let requestResponse =
            await that.fetchValidateAttachments(aFilterDocs);
          that._showMessageViewModelAttachment(requestResponse.messages);
          //console.log(requestResponse.messages);
          var arr = $.grep(requestResponse.messages, function (n, i) {
            return n.group == "SUCCESS";
          });
          arr = $.map(arr, (value) => {
            return value.subtitle;
          });
          //console.log(arr);
          oContext.aFilesSendToErp = arr;
          return;
          let idArray = [];
          if (aSelectedItems.length > 0) {
            let message = `¿Está seguro que desea enviar ${aSelectedItems.length} registro(s)?`;
            let title = "Confirmación";
            let actions = ["Si", "No"];
            let onPress = async function (option) {
              switch (option) {
                case "Si":
                  aSelectedItems.forEach(function (item) {
                    //console.log(item);
                    //let idUrl = item.__metadata.Id;
                    //console.log(idUrl)
                    //let sapId = idUrl.match(/'([^']+)'/)[1];
                    //console.log(sapId)
                    idArray.push(item.Id);
                  });
                  //console.log("TestingArray", idArray)
                  let requestResponse = await oContext.fetchSendToErp(idArray);
                  oContext._showMessageViewModel(requestResponse.messages);
                  //console.log(requestResponse.messages)

                  oContext.reloadModel();
                  break;
              }
            };

            sap.m.MessageBox.confirm(message, {
              title,
              actions,
              emphasizedAction: sap.m.MessageBox.Action.YES,
              onClose: onPress,
            });
          } else MessageToast.show("Seleccione los documentos");
        },
        _showMessageViewModelAttachment: function (messages) {
          var oContext = this;

          var oBackButton = new sap.m.Button({
            icon: "sap-icon://nav-back",
            visible: false,
            press: function () {
              oContext.oMessageView.navigateBack();
              this.setVisible(true);
            },
          });

          var oMessageTemplate = new sap.m.MessageItem({
            type: "{MessageViewModelAttachment>type}",
            title: "{MessageViewModelAttachment>title}",
            description: "{MessageViewModelAttachment>description}",
            subtitle: "{MessageViewModelAttachment>subtitle}",
            counter: "{MessageViewModelAttachment>counter}",
            groupName: "{MessageViewModelAttachment>group}",
          });

          this.oMessageView = new sap.m.MessageView({
            showDetailsPageHeader: true,
            itemSelect: function () {
              oBackButton.setVisible(true);
            },
            items: {
              path: "MessageViewModelAttachment>/",
              template: oMessageTemplate,
            },
            groupItems: true,
          });

          this.oDialog = new sap.m.Dialog({
            title: "Validacion de adjuntos",
            resizable: true,
            content: this.oMessageView,
            beginButton: new Button({
              press: function () {
                this.getParent().close();
              },
              text: "Close",
            }),
            endButton: new Button({
              press: (oEvt) => {
                oEvt.getSource().getParent().close();
                //console.log(oContext.aFilesSendToErp);
                if (oContext.aFilesSendToErp.length < 1) {
                  return MessageBox.warning(
                    "No hay documentos validos para enviar a SAP.",
                  );
                }
                oContext.onShowConfirmation(oContext.aFilesSendToErp);
              },
              text: "Enviar documento validos.",
            }),
            contentHeight: "50%",
            contentWidth: "50%",
            verticalScrolling: false,
          });

          var oModel = new JSONModel();

          oModel.setData(messages);
          oModel.setSizeLimit(messages.length);

          this.oMessageView.setModel(oModel, "MessageViewModelAttachment");

          this.oMessageView.navigateBack();

          this.oDialog.open();
        },
        _setDialogConfig: async function (name, path) {
          let oContext = this;

          let dialogs = oContext.customDialogs;

          if (!dialogs) {
            oContext.customDialogs = [];
            dialogs = oContext.customDialogs;
          }

          let dialog = oContext.loadFragment({ name: path });

          dialogs.push({
            name,
            path,
            dialog,
          });
        },

        _getDialogConfig: function (name) {
          let oContext = this;

          let dialogs = oContext.customDialogs ? oContext.customDialogs : [];

          return dialogs.find((item) => item.name === name);
        },

        _initConfigurationDialogs: async function () {
          let oContext = this;

          await oContext._setDialogConfig(
            "ImportData",
            "atriacorps4hanape.admasientocon.view.fragment.ImportData",
          );
          await oContext._setDialogConfig(
            "PreviewData",
            "atriacorps4hanape.admasientocon.view.fragment.PreviewData",
          );
        },

        _openDialogByName: async function (inputName, sName, oBody) {
          let oContext = this;

          let dialogConfig = oContext._getDialogConfig(inputName);

          dialogConfig.dialog.then((oDialog) => oDialog.open());

          if (sName) {
            oView.setModel(new JSONModel(oBody), sName);
          }
        },

        _closeDialogByName: async function (inputName) {
          let oContext = this;

          let dialogConfig = oContext._getDialogConfig(inputName);

          dialogConfig.dialog.then((oDialog) => oDialog.close());
        },

        _applicationsSwitchData: async function () {
          let applications = {
            items: [],
          };
          var endpoint = `${sDomain + sDestination}odata.svc/GlobalSettings?$format=json&$filter=GlobalSettingModule eq 'UTILIDADES' and GlobalSettingType eq 'APP_REL_DOC_VENTA' and GlobalSettingClassification eq 'CHILDREN'`;

          var myHeaders = new Headers({
            "Access-Control-Allow-Origin": "*",
          });

          var requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
          };
          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            if (responseJson.d && responseJson.d.results) {
              applications.items = responseJson.d.results.map((item) => ({
                src: item.GlobalSettingDescription,
                title: item.GlobalSettingName,
                subTitle: "",
                targetSrc: item.GlobalSettingValue,
                target: "_blank",
              }));
            }
          } catch (ex) {
            //console.error("TestingError", ex);
          }

          let oView = this.getView();
          let oModel = new JSONModel(applications);
          oView.setModel(oModel, "applicationsModel");

          let pSwitchBtn = oView.byId("pSwitchBtn");
          if (applications.items.length === 0) {
            pSwitchBtn.setVisible(false);
          } else {
            pSwitchBtn.setVisible(true);
          }
        },

        _initApplicationPopover: function () {
          let oView = this.getView();

          if (!this._pPopover) {
            this._pPopover = Fragment.load({
              id: oView.getId(),
              name: "atriacorps4hanape.admasientocon.view.fragment.ApplicationSwitch",
              controller: this,
            }).then(
              function (oPopover) {
                oView.addDependent(oPopover);
                if (Device.system.phone) {
                  oPopover.setEndButton(
                    new Button({
                      text: "Close",
                      type: "Emphasized",
                      press: this.fnClose.bind(this),
                    }),
                  );
                }
                return oPopover;
              }.bind(this),
            );
          }
        },

        fnChange: function (oEvent) {
          var oItemPressed = oEvent.getParameter("itemPressed"),
            sTargetSrc = oItemPressed.getTargetSrc();

          MessageToast.show("Redireccionando... " + sTargetSrc);

          URLHelper.redirect(sTargetSrc, true);

        },

        fnOpen: function (oEvent) {
          var oButton = this.getView().byId("pSwitchBtn");
          this._pPopover.then(function (oPopover) {
            oPopover.openBy(oButton);
          });
        },
        fnClose: function () {
          this._pPopover.then(function (oPopover) {
            oPopover.close();
          });
        },

        onPresRowDoc: (oEvent) => {
          //console.log(oEvent.getParameter("rowContext").getPath())
          that.onSetExpand(oEvent.getParameter("rowContext").getPath());
          //console.log(oEvent.getParameters("rowContext"))
        },
        onShowData: (oEvent) => {
          //console.log(oEvent);
          let sPath = oEvent
            .getSource()
            .getParent()
            .getBindingContext("ModelImport")
            .getPath();
          //console.log(sPath);
          //oView.byId("tableDetail")
          that.onSetExpand(sPath);
        },
        onSetExpand: (sPath) => {
          //oView.byId("tableDetail")

          oView.byId("tableDetail").bindRows({
            path: `ModelImport>${sPath}/cargaMasivaDocumentoVentaPeruDetalleXLSXDTOList`,
            filters: [],
          });
          oView.byId("tableRef").bindRows({
            path: `ModelImport>${sPath}/cargaMasivaDocumentoVentaPeruReferenciaXLSXDTOList`,
            filters: [],
          });
        },
        _saveDataXlsx: async () => {
          let requestBody = oView.getModel("ModelImport").getData();
          var endpoint = `${sDomain + sDestination}sales/v1/sales-document-peru/cargaMasivaDocumentoVentaPeru/xlsx/save`;
          var requestOptions = {
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(requestBody),
            method: "POST",
          };
          //console.log("TestingEndpoint", endpoint)
          try {
            let request = await fetch(endpoint, requestOptions);
            let responseJson = await request.json();
            //console.log("TestingResult", responseJson);
            return responseJson;
          } catch (ex) {
            //console.error("TestingError", ex);
          } finally {
            //console.log("TestingEnd");
            //contextGlobal._hideBusyIndicator()
          }
        },
        //async function (oEvent) {
        onClosePreviewDataAndSave: async (oEvent) => {
          //oContext
          that._initBusyIndicator();
          var documentLogs = await that._saveDataXlsx();
          //console.log(documentLogs);
          var aMessageStatus = [];
          if (documentLogs.hasOwnProperty("message")) {
            aMessageStatus.push({
              title:
                "Observacion de campos ",
              type: "Error",
              description: documentLogs.message,
              subtitle: "Carga de documentos.",
            });
          } else {
            var aMessageStatus = $.map(documentLogs, function (a) {
              return {
                title:
                  "Observacion No Doc." +
                  a.documentoVentaPeruMasivoDTO.numeroDocumentoOrigen,
                type: a.error ? "Error" : "Success",
                description: a.mensajeList.join(","),
                subtitle: "Carga de documentos.",
              };
            });
          }
          //console.log(aMessageStatus);
          that._showMessageViewModel(aMessageStatus);
          //   messages: [{
          //     title: "Operación exitosa",
          //     type: "Success",
          //     description: "Se cargaron correctamente los documentos de venta",
          //     subtitle: "Carga de documentos exitosa"
          // }],
          debugger;
          oEvent.getSource().getParent().getParent().close();
          that._hideBusyIndicator();
          that.getOwnerComponent().getModel("odata").refresh();
        },

        onClosePreviewData: async (oEvent) => {
          oEvent.getSource().getParent().getParent().close();
        },

        _showMessageViewModel: function (messages) {
          var oContext = this;

          // Crear el modelo de datos JSON
          var oModel = new sap.ui.model.json.JSONModel();
          oModel.setSizeLimit(messages.length);
          oModel.setData(messages);

          // Botón de navegación hacia atrás
          var oBackButton = new sap.m.Button({
            icon: "sap-icon://nav-back",
            visible: false,
            press: function () {
              oContext.oMessageView.navigateBack();
              this.setVisible(false);
            },
          });

          // Plantilla para los mensajes
          var oMessageTemplate = new sap.m.MessageItem({
            type: "{MessageViewModel>type}",
            title: "{MessageViewModel>title}",
            description: "{MessageViewModel>description}",
            subtitle: "{MessageViewModel>subtitle}",
            counter: "{MessageViewModel>counter}",
          });

          // Vista de mensajes
          this.oMessageView = new sap.m.MessageView({
            showDetailsPageHeader: true,
            itemSelect: function () {
              oBackButton.setVisible(true);
            },
            items: {
              path: "MessageViewModel>/",
              template: oMessageTemplate,
            },
          });

          // Asignar el modelo de datos a la vista de mensajes antes de abrir el diálogo
          this.oMessageView.setModel(oModel, "MessageViewModel");

          // Botón para generar reporte
          var oGenerateReportButton = new sap.m.Button({
            type: "Success",
            text: "Exportar",
            icon: "sap-icon://excel-attachment",
            press: this.onPressExportDataTSPPA.bind(this),
            tooltip: "Generar reporte"
          });

          // Header personalizado del diálogo
          var oDialogHeader = new sap.m.Bar({
            contentLeft: [oBackButton],
            contentMiddle: new sap.m.Text({ text: "Log de Transacción" }),
            // contentRight: [oGenerateReportButton] // Remover este botón del header
          });

          // Diálogo
          this.oDialog = new sap.m.Dialog({
            customHeader: oDialogHeader,
            resizable: true,
            content: this.oMessageView,
            buttons: [ // Cambiar beginButton por buttons
              oGenerateReportButton, // Agregar botón de reporte al footer
              new sap.m.Button({
                press: function () {
                  this.getParent().close();
                },
                text: "Close",
              })
            ],
            contentHeight: "50%",
            contentWidth: "50%",
            verticalScrolling: false,
          });

          // Asegurar que la vista de mensajes navegue al estado inicial
          this.oMessageView.navigateBack();

          // Abrir el diálogo
          this.oDialog.open();
        },

        //Generar Reporte
        onPressExportDataTSPPA: async function () {
          this._initBusyIndicator();

          try {
            let oMessageModel = this.oMessageView.getModel("MessageViewModel");

            if (!oMessageModel) {
              //console.error("El modelo MessageViewModel no está definido.");
              MessageToast.show("El modelo de mensajes no está disponible.");
              return;
            }

            let aData = oMessageModel.getData();

            if (aData && aData.length > 0) {
              let aExportData = aData.map((o) => this._buildDataReportTSPPA(o));
              this.fileManager.generateExcel(aExportData, "Reporte del Log de Transacción");
            } else {
              MessageToast.show("No hay datos disponibles para exportar.");
            }
          } catch (oError) {
            //console.log(oError);
          } finally {
            this._hideBusyIndicator();
          }
        },


        _buildDataReportTSPPA: function (o) {
          return {
            "Tipo": o.type,
            "Título": o.title,
            "Descripción": o.description,
            "Subtítulo": o.subtitle,
          };
        },
      },
    );
  },
);
