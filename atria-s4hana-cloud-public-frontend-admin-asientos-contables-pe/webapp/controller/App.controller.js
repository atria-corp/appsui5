sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function (BaseController) {
        "use strict";
        var that;
        return BaseController.extend("atriacorps4hanape.monitordocventas.controller.App", {
            onInit: function () {
                debugger
                that = this;
                this.oRouter = this.getOwnerComponent().getRouter();
                this.oRouter.attachRouteMatched(this.onRouteMatched, this);
                this.oRouter.attachBeforeRouteMatched(this.onBeforeRouteMatched, this);

            },

            onBeforeRouteMatched: function (oEvent) {
                debugger
                var oModel = this.getOwnerComponent().getModel();

                var sLayout = oEvent.getParameters().arguments.layout;
                console.log("APP js", oEvent.getParameters())
                // If there is no layout parameter, query for the default level 0 layout (normally OneColumn)
                if (!sLayout) {
                    var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(0);
                    sLayout = oNextUIState.layout;
                }

                // Update the layout of the FlexibleColumnLayout
                if (sLayout) {
                    oModel.setProperty("/layout", sLayout);
                }
            },
            onShowTwoColumn: function (oParam) {
                console.log("App js" + oParam)
                debugger
                var oModel = that.getOwnerComponent().getModel();

                oModel.setProperty("/layout", "TwoColumnsMidExpanded");
                that._updateUIElements();

            },
            onRouteMatched: function (oEvent) {
                debugger
                var sRouteName = oEvent.getParameter("name"),
                    oArguments = oEvent.getParameter("arguments");

                this._updateUIElements();

                // Save the current route name
                this.currentRouteName = sRouteName;
                // this.currentProduct = oArguments.product;
                // this.currentSupplier = oArguments.supplier;
            },

            onStateChanged: function (oEvent) {
                var bIsNavigationArrow = oEvent.getParameter("isNavigationArrow"),
                    sLayout = oEvent.getParameter("layout");

                this._updateUIElements();

                // Replace the URL with the new layout if a navigation arrow was used
                if (bIsNavigationArrow) {
                    this.oRouter.navTo(this.currentRouteName, { layout: sLayout, supplier: 'todo', supplier: 'todo1' }, true);
                }
            },

            // Update the close/fullscreen buttons visibility
            _updateUIElements: function () {
                var oModel = this.getOwnerComponent().getModel();
                var oUIState = this.getOwnerComponent().getHelper().getCurrentUIState();
                oModel.setData(oUIState);
            },

            onExit: function () {
                this.oRouter.detachRouteMatched(this.onRouteMatched, this);
                this.oRouter.detachBeforeRouteMatched(this.onBeforeRouteMatched, this);
            }

        });
    }
);
