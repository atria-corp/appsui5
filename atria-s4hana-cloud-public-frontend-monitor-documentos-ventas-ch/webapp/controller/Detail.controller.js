sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox"
], function (JSONModel, Controller, MessageBox) {
    "use strict";
    //
    var globalCurrentDocument
    var contextGlobal

    return Controller.extend("atriacorps4hanach.monitordocventas.controller.Detail", {
        onInit: async function () {

            contextGlobal = this

            var oExitButton = this.getView().byId("exitFullScreenBtn")
            var oEnterButton = this.getView().byId("enterFullScreenBtn")

            this.oRouter = this.getOwnerComponent().getRouter()
            this.oModel = this.getOwnerComponent().getModel()
            console.log(this.getView())
            this.oRouter.getRoute("detail").attachPatternMatched(this._bindingDocumentMatcher, this);

            [oExitButton, oEnterButton].forEach(function (oButton) {
                oButton.addEventDelegate({
                    onAfterRendering: function () {
                        if (this.bFocusFullScreenButton) {
                            this.bFocusFullScreenButton = false;
                            oButton.focus();
                        }
                    }.bind(this)
                });
            }, this);

        },

        handleFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.NumeroDocumento });
        },
        handleExitFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.NumeroDocumento });
        },
        handleClose: function () {
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
            this.oRouter.navTo("list", { layout: sNextLayout });
        }, _bindingDocumentMatcher: async function (oEvent) {

            //--- Obtenemos la view
            debugger
            let oView = this.getView()

            //--- Obtenemos la posición dentro del array del proveedor seleccionado
            const iNumDoc = oEvent.getParameter("arguments").supplier || 0

            console.log("Testing", oView.getModel("odata"))
            console.log("Testing2", oView.getModel("odata").oData)

            const oData = oView.getModel("odata").oData

            //globalCurrentDocument = Object.values(oData).find( (item) => item.NumeroDocumento == NumeroDocumento )


            oView.byId("detailsTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaDetalleDetails`,//DocumentoVentas(89)/DocumentoVentaDetalleDetails
                filters: []
            });
            oView.byId("referenceTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaReferenciaDetails`,//DocumentoVentas(89)/DocumentoVentaReferenciaDetails
                filters: []
            });
            oView.byId("logTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaLogDetails`,//DocumentoVentas(89)/DocumentoVentaLogDetails
                filters: []
            });

            //var documentLogs = await this._fetchDocumentLogs(iNumDoc)
            var oDocument = await this._fetchDocument(iNumDoc)
            // oView.byId("idTimeline").bindContent({
            //     path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaLogDetails`,//DocumentoVentas(89)/DocumentoVentaLogDetails
            //     filters: []
            // });
            //oView.byId("idTimeline").bindContext(`odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaLogDetails`);
            //let documentoId = globalCurrentDocument.NumeroDocumento;
            //let modelDocumentData = new JSONModel( globalCurrentDocument )

            //oView.setModel( {}, "documentData" )
            //console.log(this.getView())
            // console.log(documentLogs)

            // var documentDetails = await this._fetchDocumentDetails(documentoId)

            // var documentReferences = await this._fetchDocumentReference(documentoId)

            // var documentLogs = await this._fetchDocumentLogs(documentoId)

            // let detailModel = new JSONModel(documentDetails.d.results)
            // oView.setModel(detailModel, "documentsDetails")
            // //detailModel.attachRequestCompleted(processFlowComponent.updateModel.bind(processFlowComponent))

            // let referenceModel = new JSONModel(documentReferences.d.results)
            // oView.setModel(referenceModel, "documentsReference")

            //let logModel = new JSONModel(documentLogs.d.results)
            oView.setModel(new JSONModel(oDocument.d), "documentData")
            //oView.setModel(new JSONModel(documentLogs.d.results), "documentsLogs")
            //oView.getModel("documentsLogs").refresh();

        }, _fetchDocumentDetails: async (sapId) => {
            contextGlobal._initBusyIndicator()

            var endpoint = "/s4hana-extensibility/odata.svc/DetalleVentaSigges?$format=json&$filter=NumeroDocumento eq '" + sapId + "'";

            var myHeaders = new Headers({
                "Access-Control-Allow-Origin": "*",
            });

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            console.log("TestingEndpoint", endpoint)
            try {
                let request = await fetch(endpoint, requestOptions)
                let responseJson = await request.json()
                console.log("TestingResult", responseJson)
                return responseJson
            } catch (ex) {
                console.error("TestingError", ex)
            } finally {
                console.log("TestingEnd")
                contextGlobal._hideBusyIndicator()
            }
        }, _fetchDocumentReference: async (sapId) => {
            contextGlobal._initBusyIndicator()
            var endpoint = "/s4hana-extensibility/odata.svc/Referencias?$format=json&$filter=NumeroDocumento eq '" + sapId + "'";
            let responseJson;

            var myHeaders = new Headers({
                "Access-Control-Allow-Origin": "*",
            });

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            console.log("TestingEndpoint", endpoint)
            try {
                let request = await fetch(endpoint, requestOptions)
                responseJson = await request.json()
                console.log("TestingResult", responseJson)
                return responseJson
            } catch (ex) {
                console.error("TestingError", ex)
            } finally {
                console.log("TestingEnd")
                contextGlobal._hideBusyIndicator()
            }
        }, _fetchDocumentLogs: async (sapId) => {
            contextGlobal._initBusyIndicator()
            console.log("TestingInside")
            //var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            var endpoint = `/s4hana-extensibility/odata.svc/DocumentoVentas(${sapId})/DocumentoVentaLogDetails?$format=json`;

            var myHeaders = new Headers({
                "Access-Control-Allow-Origin": "*",
            });

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            console.log("TestingEndpoint", endpoint)
            try {
                let request = await fetch(endpoint, requestOptions)
                let responseJson = await request.json()
                console.log("TestingResult", responseJson)
                //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
                return responseJson
            } catch (ex) {
                console.error("TestingError", ex)
            } finally {
                console.log("TestingEnd")
                contextGlobal._hideBusyIndicator()
            }
        }, _fetchDocument: async (sapId) => {
            contextGlobal._initBusyIndicator()
            console.log("TestingInside")
            //var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            var endpoint = `/s4hana-extensibility/odata.svc/DocumentoVentas(${sapId})?$format=json`;

            var myHeaders = new Headers({
                "Access-Control-Allow-Origin": "*",
            });

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            console.log("TestingEndpoint", endpoint)
            try {
                let request = await fetch(endpoint, requestOptions)
                let responseJson = await request.json()
                console.log("TestingResult", responseJson)
                //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
                return responseJson
            } catch (ex) {
                console.error("TestingError", ex)
            } finally {
                console.log("TestingEnd")
                contextGlobal._hideBusyIndicator()
            }
        },
        _buildProcessFlowInput: (detailList) => {
            let position = 0
            let listSize = detailList.length
            console.log("TestingSize", listSize)
            let lanes = []
            let nodes = []

            detailList.forEach(item => {
                console.log("TestingItem", item)
            })
            return { "lanes": lanes, "nodes": nodes }
        }, _initBusyIndicator: () => {
            sap.ui.core.BusyIndicator.show()
        }, _hideBusyIndicator: () => {
            sap.ui.core.BusyIndicator.hide()
        },
    });
});