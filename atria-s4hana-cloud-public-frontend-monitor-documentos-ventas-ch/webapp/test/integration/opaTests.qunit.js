/* global QUnit */

sap.ui.require(["atriacorps4hanach/monitordocventas/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
