sap.ui.define([], () => {
	"use strict";

	return {
        configIcon: (bError)=> {
            return bError ? "sap-icon://error"  : "sap-icon://sys-enter-2" ;
        },
		statusText: (sText)=> {
			console.log(sText)
             return sText;   //return bAvailable ? "Success" : "Error";
		},
        formatAvailableToObjectState: function(bAvailable) {
            console.log(bAvailable)
            return bAvailable ? "Success" : "Error";
        },
        formatMessage: function(aMessage) {
            console.log(aMessage)
            let sMessage = aMessage ? aMessage.join(", "): "";
            return sMessage.length > 0 ? sMessage : "Sin observacion.";
            
        },
	};
});