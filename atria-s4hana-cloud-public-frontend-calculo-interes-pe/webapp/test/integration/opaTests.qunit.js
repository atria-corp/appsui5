/* global QUnit */

sap.ui.require(["atriacorps4hanape/calcinteres/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
