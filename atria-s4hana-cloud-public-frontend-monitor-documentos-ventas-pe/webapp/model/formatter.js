sap.ui.define([
    "sap/ui/core/format/DateFormat"
], (DateFormat) => {
	"use strict";

	return {
        configIcon: (bError)=> {
            return bError ? "sap-icon://error"  : "sap-icon://sys-enter-2" ;
        },
		statusText: (sText)=> {
			console.log(sText)
             return sText;   //return bAvailable ? "Success" : "Error";
		},
        formatAvailableToObjectState: function(bAvailable) {
            console.log(bAvailable)
            return bAvailable ? "Success" : "Error";
        },
        formatMessage: function(aMessage) {
            console.log(aMessage)
            let sMessage = aMessage ? aMessage.join(", "): "";
            return sMessage.length > 0 ? sMessage : "Sin observacion.";
            
        },
        formatDate: function(oDateValue) {

            console.log("Fecha original:", oDateValue);
            if (!oDateValue) {
                return "";
            }
            var timestamp = parseInt(oDateValue.replace("/Date(", "").replace(")/", ""), 10);
            var oDate = new Date(timestamp);
            var oDateFormat = DateFormat.getDateTimeInstance({
                pattern: "dd/MM/yyyy"
            });
            var formattedDate = oDateFormat.format(oDate);
            console.log("Fecha formateada:", formattedDate);
            return formattedDate;
        },
        formatDatev2: function(oDateValue) {
            debugger
            
            var oDate = new Date(oDateValue);
            var oDateFormat = DateFormat.getDateTimeInstance({
                pattern: "dd/MM/yyyy"
            });
            var formattedDate = oDateFormat.format(oDate);
            console.log("Fecha formateada:", formattedDate);
            return formattedDate;
        }
	};
});