sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/core/format/DateFormat",
    "../model/formatter"
], function (JSONModel, Controller, MessageBox, DateFormat, formatter) {
    "use strict";
//
    var globalCurrentDocument
	var contextGlobal
    var sDomain = "";
    var sDestination = "s4hana-extensibility/";
    var sPrefixOdata = "";

    return Controller.extend("atriacorps4hanape.monitordocventas.controller.Detail", {
        formatter: formatter,
        onInit: async function () {

            contextGlobal = this
            // contextGlobal._initBusyIndicator();
            var oExitButton = this.getView().byId("exitFullScreenBtn")
            var oEnterButton = this.getView().byId("enterFullScreenBtn")

            sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
            sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
            sDomain = sDomain.length > 0 ? sDomain : "./";

            this.oRouter = this.getOwnerComponent().getRouter()
            this.oModel = this.getOwnerComponent().getModel()
            //console.log(this.getView())
            this.oRouter.getRoute("detail").attachPatternMatched(this._bindingDocumentMatcher, this);

            [oExitButton, oEnterButton].forEach(function (oButton) {
                oButton.addEventDelegate({
                    onAfterRendering: function () {
                        if (this.bFocusFullScreenButton) {
                            this.bFocusFullScreenButton = false;
                            oButton.focus();
                            // contextGlobal._hideBusyIndicator();
                        }
                    }.bind(this)
                });
            }, this);

        },

        handleFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.Id });
        },

        handleExitFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.Id });
        },

        handleClose: function () {
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
            this.oRouter.navTo("list", { layout: sNextLayout });
        },
        
        _bindingDocumentMatcher: async function (oEvent) {
            //--- Obtenemos la view
            debugger;
            let oView = this.getView();
        
            //--- Obtenemos la posición dentro del array del proveedor seleccionado
            const iNumDoc = oEvent.getParameter("arguments").supplier || 0;
        
            const oData = oView.getModel("odata").oData;
        
            // Vincular tablas
            oView.byId("detailsTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaDetalleDetails`,
                filters: []
            });
            oView.byId("referenceTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaReferenciaDetails`,
                filters: []
            });
            oView.byId("logTable").bindRows({
                path: `odata>/DocumentoVentas(${iNumDoc})/DocumentoVentaLogDetails`,
                filters: []
            });
        
            try {
                // Obtener datos del documento
                var oDocument = await this._fetchDocument(iNumDoc);
                var oDateFormat = DateFormat.getDateTimeInstance({ pattern: "dd/MM/yyyy" });
        
                // Formatear fechas del documento
                oDocument.d.DocumentoFechaEmision = this._formatDate(oDocument.d.DocumentoFechaEmision, oDateFormat);
                oDocument.d.DocumentoFechaVencimiento = this._formatDate(oDocument.d.DocumentoFechaVencimiento, oDateFormat);
        
                // Establecer modelo "documentData"
                oView.setModel(new JSONModel(oDocument.d), "documentData");
        
                // Obtener y establecer logs del documento si existe la función
                if (this._fetchDocumentLogs) {
                    var documentLogs = await this._fetchDocumentLogs(iNumDoc);
                    oView.setModel(new JSONModel(documentLogs.d.results), "documentsLogs");
                }
            } catch (error) {
                console.error("Error al obtener los datos del documento:", error);
            }
        },
        

        _formatDate: function(oDateValue, oDateFormat) {
            if (!oDateValue) {
                return "";
            }
            var timestamp = parseInt(oDateValue.replace("/Date(", "").replace(")/", ""), 10);
            var oDate = new Date(timestamp);
            return oDateFormat.format(oDate);
        },

        _fetchDocumentDetails: async (sapId) => {
			contextGlobal._initBusyIndicator()
            
            var endpoint = "/s4hana-extensibility/odata.svc/DetalleVentaSigges?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            //console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				let responseJson = await request.json()
                //console.log("TestingResult", responseJson)
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                //console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},
        
        _fetchDocumentReference: async (sapId) => {
			contextGlobal._initBusyIndicator()
			var endpoint = "/s4hana-extensibility/odata.svc/Referencias?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            let responseJson;

			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            //console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				responseJson = await request.json()
                //console.log("TestingResult", responseJson)
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                //console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},
        
        _fetchDocumentLogs: async (sapId) => {
			contextGlobal._initBusyIndicator()
            //console.log("TestingInside")
			//var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            var endpoint = `/s4hana-extensibility/odata.svc/DocumentoVentas(${sapId})/DocumentoVentaLogDetails?$format=json`;
            
			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            //console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				let responseJson = await request.json()
                //console.log("TestingResult", responseJson)
                //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                //console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},
        
        _fetchDocument: async (sapId) => {
			contextGlobal._initBusyIndicator()
            //console.log("TestingInside")
			//var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            var endpoint = `${sDomain + sDestination}odata.svc/ViewDocumentoVentas(${sapId})?$format=json`;
            
			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            //console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				let responseJson = await request.json()
                //console.log("TestingResult", responseJson)
                //oView.setModel(new JSONModel(responseJson.d.results), "documentsLogs")
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                //console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},

        _buildProcessFlowInput: (detailList) => {
			let position = 0
			let listSize = detailList.length
            //console.log("TestingSize",listSize)
			let lanes = []
			let nodes = []

			detailList.forEach(item => {
                //console.log("TestingItem",item)
			})
			return { "lanes": lanes, "nodes": nodes }
		}
        
        ,_initBusyIndicator: () => {
			sap.ui.core.BusyIndicator.show()
		}
        
        ,_hideBusyIndicator: () => {
			sap.ui.core.BusyIndicator.hide()
		},
    });
});