/* global QUnit */

sap.ui.require(["atriacorps4hanach/mantbp/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
