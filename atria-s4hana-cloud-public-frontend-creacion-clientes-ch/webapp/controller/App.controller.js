sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("atriacorps4hanach.mantbp.controller.App", {
        onInit: function() {
        }
      });
    }
  );
  