sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/m/library",
    "sap/m/Button",
    'sap/m/Bar',
    'sap/m/Title',
    'sap/m/Popover',
    "sap/ui/Device",
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel 
     * @param {typeof sap.m.MessageToast} MessageToast 
     * @param {typeof sap.ui.core.Fragment} Fragment 
     * @param {typeof sap.m.library} library 
     * @param {typeof sap.m.Button} Button 
     * @param {typeof sap.m.Bar} Bar 
     * @param {typeof sap.ui.Device} Device 
     */
    function (Controller, JSONModel, MessageToast, Fragment, library, Button, Bar, Title, Popover, Device) {
        "use strict";
        var sDomain = "";
        var sDestination = "s4hana-extensibility/";
        var sPrefixOdata = "";
        return Controller.extend("atriacorps4hanach.mantbp.controller.PrincipalPage", {

            onInit: function () {
                var oContext = this;
                this._oButton = this.byId("crearBtn");
                oContext.byId('dynamicPageId').setShowFooter(true);
                
                
                // Inicializar _formFragments como un objeto vacío
                this._formFragments = {};
                
                // Configuración del dominio y prefijo del servicio
                sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
                sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
                sDomain = sDomain.length > 0 ? sDomain : "./";
                
                // Inicializar modelos y cargar settings globales
                let oView = oContext.getView();
                if (!oView.getModel("Clientes")) {
                    let oProveedorModel = new JSONModel({
                        pais: "CL",
                        lang: "ES",
                        tipoBP: [],
                        enabled: false // Inicialmente deshabilitado
                    });
                    oView.setModel(oProveedorModel, "Clientes");
                }
                
                this._applicationsSwitchData()
                this._initApplicationPopover()
                // Cargar el fragmento inicial
                this._showFormFragment("ClientDisplay", false);
            
                var deferred = $.Deferred();
                var oDataModel = this.getOwnerComponent().getModel("odata");
                oDataModel.read("/GlobalSettings", {
                    success: function (oData) {
                        var filteredData = oData.results.filter(function (item) {
                            return item.GlobalSettingType === "ROLES_BUSINESS_PARTNER" &&
                                item.GlobalSettingClassification === "CHILDREN";
                        });
                        deferred.resolve(filteredData);
                    },
                    error: function (oError) {
                        console.error("Error fetching data:", oError);
                        deferred.reject(oError);
                    }
                });
            
                deferred.done(function (filteredData) {
                    if (!oView.getModel("GlobalSettingsBP")) {
                        let oRoleModelData = { Roles: filteredData };
                        let oRoleModel = new JSONModel(oRoleModelData);
                        oView.setModel(oRoleModel, "GlobalSettingsBP");
                    }
                });
            },
            
            

            _initBusyIndicator: () => {

                sap.ui.core.BusyIndicator.show(0)

            },

            _hideBusyIndicator: () => {

                sap.ui.core.BusyIndicator.hide()

            },

            _initApplicationPopover: function () {
                let oView = this.getView()

                if (!this._pPopover) {
                    this._pPopover = Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hanach.mantbp.view.fragments.ApplicationSwitch",
                        controller: this
                    }).then(function (oPopover) {
                        oView.addDependent(oPopover);
                        if (Device.system.phone) {
                            oPopover.setEndButton(new Button({ text: "Close", type: "Emphasized", press: this.fnClose.bind(this) }));
                        }
                        return oPopover;
                    }.bind(this));
                }
            },

            fnOpen: function () {
                var oButton = this.getView().byId("pSwitchBtn");
                this._pPopover.then(function (oPopover) {
                    oPopover.openBy(oButton);
                });
            },

            fnClose: function () {
                this._pPopover.then(function (oPopover) {
                    oPopover.close();
                });
            },

            _applicationsSwitchData: async function () {
                debugger
                let applications = {
                    "items": []
                };
                console.log(`${sDomain + sDestination}`)
                var endpoint = `${sDomain + sDestination}odata.svc/GlobalSettings?$format=json&$filter=GlobalSettingModule eq 'UTILIDADES' and GlobalSettingType eq 'APP_REL_MANT_BP_CL' and GlobalSettingClassification eq 'CHILDREN'`;

                var myHeaders = new Headers({
                    "Access-Control-Allow-Origin": "*",
                });

                var requestOptions = {
                    method: 'GET',
                    headers: myHeaders,
                    redirect: 'follow'
                };
                try {
                    let request = await fetch(endpoint, requestOptions);
                    let responseJson = await request.json();
                    if (responseJson.d && responseJson.d.results) {
                        applications.items = responseJson.d.results.map(item => ({
                            "src": item.GlobalSettingDescription,
                            "title": item.GlobalSettingName,
                            "subTitle": "",
                            "targetSrc": item.GlobalSettingValue,
                            "target": "_blank"
                        }));
                    }
                } catch (ex) {
                    console.error("TestingError", ex);
                }

                let oView = this.getView();
                let oModel = new JSONModel(applications);
                oView.setModel(oModel, "applicationsModel");

                let pSwitchBtn = oView.byId("pSwitchBtn");
                if (applications.items.length === 0) {
                    pSwitchBtn.setVisible(false);
                } else {
                    pSwitchBtn.setVisible(true);
                }
            },

            handlePopoverPress: function (oEvent) {
                this.oMessageView.navigateBack();
                this._oPopover.openBy(oEvent.getSource());
                this.byId("mVBtn").setType("Default")
            },

            findClientByRUT: async function () {
                try {
                    let oContext = this;
                    oContext.byId('overFlowTBar').setVisible(true);
            
                    let rut = oContext.byId('rutInput').getValue();
                    let requestResponse = {};
            
                    if (rut) {
                        requestResponse = await oContext.fetchSupplierERPByRUT(rut);
                    } else {
                        let oProveedorModel = new JSONModel();
                        oProveedorModel.setProperty("/pais", "CL");
                        oProveedorModel.setProperty("/lang", "ES");
                        oProveedorModel.setProperty("/tipoBP", []);
                        this.getView().setModel(oProveedorModel, "Clientes");
            
                        requestResponse = {
                            success: false,
                            messages: [{
                                title: "Información incompleta",
                                type: "Error",
                                description: `No se ha ingresado un RUT.`,
                                subtitle: "Información no enviada"
                            }],
                            response: {}
                        };
                    }
            
                    oContext._showMessageViewModel(requestResponse.messages);
            
                    if (requestResponse.success) {
                        oContext.reloadModel();
                        // Actualizar la propiedad enabled en el modelo
                        this.getView().getModel("Clientes").setProperty("/enabled", true);
                    }
            
                    console.log(requestResponse);
                } catch (error) {
                    console.error("Error en findClientByRUT:", error);
                }
            },
              
            

            fetchSupplierERPByRUT: async function (rutCliente) {
                this._initBusyIndicator();
                var endpoint = `${sDomain + sDestination}integration/s4hana-cloud/v1/business-partner/generic/${rutCliente}`;
                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                };
            
                let response = {};
            
                try {
                    let request = await fetch(endpoint, requestOptions);
                    if (!request.ok) {
                        switch (request.status) {
                            case 404:
                                throw new Error('Business Partner no encontrado.');
                            case 500:
                                throw new Error('Error del servidor interno.');
                            default:
                                throw new Error('Error en la red. Estado: ' + request.status);
                        }
                    }
            
                    let responseBody = await request.json();
                    let pais = this.getView().getModel("Clientes").getProperty("/pais");
            
                    if (responseBody.response && responseBody.response.length !== 0) {
                        if (pais === "PE") {
                            response = await this.fetchClientByRUC(rutCliente, true);
                        } else {
                            response = await this.fetchClientByRUT(rutCliente, true);
                        }
                    } else {
                        if (pais === "PE") {
                            response = await this.fetchClientByRUC(rutCliente, false);
                        } else {
                            response = await this.fetchClientByRUT(rutCliente, false);
                        }
                    }
                } catch (error) {
                    console.error('Fetch error: ', error);
            
                    response = {
                        success: false,
                        messages: [{
                            title: `${error.title}`,
                            type: `${error.type}`,
                            description: `${error.description}`,
                            subtitle: `${error.subtitle}`,
                        }],
                        response: {}
                    };
                } finally {
                    this._hideBusyIndicator();
                }
            
                return response;
            },
            

            fetchClientByRUC: async function (rutCliente, existing) {
                this._initBusyIndicator();
                var endpoint = `${sDomain + sDestination}api/contribuyente/${rutCliente}`;
                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                };

                let response = {};

                try {
                    let request = await fetch(endpoint, requestOptions);
                    if (!request.ok) {
                        throw new Error('Error en la red. Estado: ' + request.status);
                    }

                    let responseBody = await request.json();
                    if (responseBody.ruc !== rutCliente) {
                        this.byId('crearBtn').setEnabled(false);
                        let oProveedorModel = new JSONModel();
                        if (!oProveedorModel.getProperty("/pais")) {
                            oProveedorModel.setProperty("/pais", "PE");
                        }
                        if (!oProveedorModel.getProperty("/lang")) {
                            oProveedorModel.setProperty("/lang", "ES");
                        }
                        if (!oProveedorModel.getProperty("/tipoBP")) {
                            oProveedorModel.setProperty("/tipoBP", []);
                        }
                        this.getView().setModel(oProveedorModel, "Clientes");
                        throw new Error('El RUC ingresado no fue encontrado');
                    } else {
                        let message = this.insertData(responseBody, existing);
                        response = {
                            success: true,
                            messages: [{
                                title: "Operación exitosa",
                                type: "Success",
                                description: message,
                                subtitle: "El usuario ha sido encontrado correctamente."
                            }],
                            response: {}
                        };
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `No se pudo completar la solicitud: ${error.message}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    };
                } finally {
                    this._hideBusyIndicator();
                }
                return response;
            },

            fetchClientByRUT: async function (rutCliente, existing) {
                this._initBusyIndicator();
                var endpoint = `${sDomain + sDestination}consulta?rut=${rutCliente}`;
                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                };

                let response = {};

                try {
                    let request = await fetch(endpoint, requestOptions);
                    let responseBody = await request.json();

                    if (!request.ok) {
                        console.log("error:")
                        console.log(responseBody)
                        throw new Error(responseBody.error);
                    }
                    let message = this.insertData(responseBody, existing);
                    response = {
                        success: true,
                        messages: [{
                            title: "Operación exitosa",
                            type: "Success",
                            description: message,
                            subtitle: "El usuario ha sido encontrado correctamente."
                        }],
                        response: responseBody
                    };
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: error,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    };
                } finally {
                    this._hideBusyIndicator();
                }

                return response;
            },




            onRoleMCBSelectionChange: function (oEvent) {
                console.log("Seleccionado ROL", oEvent)
                console.log("Source ROL", oEvent.getSource())
                console.log("Testing ROL", oEvent.getParameter("changedItems"))
                var aSelectedItems = oEvent.getParameter("changedItems");
                var aRoleValues = [];

                if (aSelectedItems) {
                    aSelectedItems.forEach(function (oItem) {
                        var sRoleValue = oItem.getKey();
                        aRoleValues.push(sRoleValue);
                    });
                }

                var oProveedorModel = this.getView().getModel("Clientes");
                oProveedorModel.setProperty("/tipoBP", aRoleValues);
            },

            onRoleMCBSelectionFinish: function (oEvent) {
                console.log("Seleccionado ROL", oEvent)
                console.log("Source ROL", oEvent.getSource())
                console.log("Testing ROL", oEvent.getParameter("selectedItems"))
                var aSelectedItems = oEvent.getParameter("selectedItems");
                var aRoleValues = [];

                if (aSelectedItems) {
                    aSelectedItems.forEach(function (oItem) {
                        var sRoleValue = oItem.getKey();
                        aRoleValues.push(sRoleValue);
                    });
                }

                var oProveedorModel = this.getView().getModel("Clientes");
                oProveedorModel.setProperty("/tipoBP", aRoleValues);
                this.getView().setModel(oProveedorModel, "Clientes");
            },

            insertData: function (client, existing) {
                var oModel = this.getView().getModel("Clientes");
                oModel.setData(client);
                if (!oModel.getProperty("/pais")) {
                    oModel.setProperty("/pais", "CL");
                }
                if (!oModel.getProperty("/lang")) {
                    oModel.setProperty("/lang", "ES");
                }
                if (!oModel.getProperty("/tipoBP")) {
                    oModel.setProperty("/tipoBP", []);
                }
                this.getView().setModel(oModel, "Clientes");
            
                this.byId('dynamicPageId').setShowFooter(true);
                this.byId('overFlowTBar').setVisible(true);
                this.byId('crearBtn').setEnabled(!existing);
            
                return "Los datos del cliente han sido cargados.";
            },
            

            reloadModel: function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("Clientes")
                if (oModel) {
                    oModel.refresh()
                } else {
                    console.error("Model not found");
                }
            },

            saveBPtoERP: async function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("Clientes");
                let oData = oModel.getData();
                let data = {};
                console.log("Data", oData)
                if (oData.pais === 'PE') {
                    data = {
                        "fullName": oData.razonSocial,
                        "category": "2", //Burned
                        "grouping": "BP02", //Burned
                        "country": oData.pais,
                        "houseNumber": oData.numero,
                        "language": oData.lang,
                        "postalCode": oData.ubigeo,
                        "streetName": `${oData.tipoVia} ${oData.nombreVia}`,
                        "interior": oData.interior,
                        "kilometer": oData.kilometro,
                        "cityBlock": oData.manzana,
                        "lot": oData.lote,
                        "codeZone": oData.codigoZona,
                        "typeZone": oData.tipoZona,
                        "status": oData.estado,
                        "conditionDom": oData.condicionDomicilio,
                        "taxType": "PE1", //Burned
                        "taxNumber": oData.ruc,
                        "codesTypeBusinessPartner": oData.tipoBP
                    }
                } else {
                    data = {
                        "fullName": oData.razonSocial,
                        "category": "2", //Burned
                        "grouping": "BP02", //Burned
                        "country": oData.pais,
                        "language": oData.lang,
                        "taxType": "CL1", //Burned
                        "taxNumber": oData.rut,
                        "activities": oData.actividades,
                        "codesTypeBusinessPartner": oData.tipoBP
                    }
                }
                console.log("ROLES SELECCIONADOS", oData.tipoBP)

                const confirmMessage = oData.tipoBP.length === 0
                    ? "No ha seleccionado algún rol para el Business Partner. ¿Desea seguir con la creación en el ERP?"
                    : "¿Desea crear al Business Partner en el ERP?";

                sap.m.MessageBox.confirm(confirmMessage, {
                    title: "Confirmación de creación",
                    onClose: async function (oAction) {
                        if (oAction === sap.m.MessageBox.Action.OK) {
                            let requestResponse = await oContext.fetchSaveBPtoERP(data);
                            oContext._showMessageViewModel(requestResponse.messages);
                            if (requestResponse.success) {
                                oContext.reloadModel();
                                oContext._oButton.setEnabled(false); // Disable the button on success
                            }
                            console.log(requestResponse);
                        } else {
                            console.log("Operation canceled by user");
                        }
                    }
                });
            },

            fetchSaveBPtoERP: async function (data) {
                this._initBusyIndicator();

                console.log("Data to save", data);
                var country = "peru"
                if (data.country === "CL")
                    country = "chile"

                var endpoint = `${sDomain + sDestination}masterdata/v1/business-partner/${country}/save`;

                const requestOptions = {
                    method: "POST",
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    redirect: "follow"
                };

                let response = {}

                try {
                    let request = await fetch(endpoint, requestOptions)
                    console.log(request)
                    let response = await request.json()
                    console.log("Testing", response)
                    response.success = true;
                    return response
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }

                } finally {
                    this._hideBusyIndicator()
                }
                return response
            },

            _showMessageViewModel: function (messages) {
                var oContext = this;

                var oBackButton = new sap.m.Button({
                    icon: "sap-icon://nav-back",
                    visible: false,
                    press: function () {
                        oContext.oMessageView.navigateBack();
                        oContext._oPopover.focus();
                        this.setVisible(false);
                    }
                });

                var oMessageTemplate = new sap.m.MessageItem({
                    type: '{MessageViewModel>type}',
                    title: '{MessageViewModel>title}',
                    description: '{MessageViewModel>description}',
                    subtitle: '{MessageViewModel>subtitle}',
                    counter: '{MessageViewModel>counter}',
                });


                this.oMessageView = new sap.m.MessageView({
                    showDetailsPageHeader: false,
                    itemSelect: function () {
                        oBackButton.setVisible(true);
                    },
                    items: {
                        path: "MessageViewModel>/",
                        template: oMessageTemplate
                    }
                });

                var oCloseButton = new Button({
                    text: "Cerrar",
                    press: function () {
                        oContext._oPopover.close();
                    }
                }).addStyleClass("sapUiTinyMarginEnd"),
                    oPopoverFooter = new Bar({
                        contentRight: oCloseButton
                    }),
                    oPopoverBar = new Bar({
                        contentLeft: [oBackButton],
                        contentMiddle: [
                            new Title({ text: "Log de Transacción" })
                        ]
                    });

                this._oPopover = new Popover({
                    customHeader: oPopoverBar,
                    contentWidth: "440px",
                    contentHeight: "440px",
                    verticalScrolling: false,
                    modal: true,
                    content: [this.oMessageView],
                    footer: oPopoverFooter
                });

                var oModel = new JSONModel();

                oModel.setData(messages);

                this.oMessageView.setModel(oModel, "MessageViewModel");

                this.oMessageView.navigateBack();
                console.log("Button Messages", messages)
                if (messages) {
                    if (messages[0].type === 'Error')
                        this.byId("mVBtn").setType("Critical");
                    else if (messages[0].type === 'Success')
                        this.byId("mVBtn").setType("Success");
                    else if (messages[0].type === 'Information')
                        this.byId("mVBtn").setType("Neutral")
                } else
                    this.byId("mVBtn").setType("Critical")
            },

            _toggleButtonsAndView: function (bEdit) {
                var oView = this.getView();

                // Show the appropriate action buttons
                oView.byId("edit").setVisible(!bEdit);
                oView.byId("save").setVisible(bEdit);
                oView.byId("cancel").setVisible(bEdit);

                // Set the right form type
                this._showFormFragment(bEdit ? "ClientChange" : "ClientDisplay");
            },

            _getFormFragment: function (sFragmentName) {
                var oView = this.getView();

                if (!this._formFragments[sFragmentName]) {
                    this._formFragments[sFragmentName] = Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hanach.mantbp.view.fragments." + sFragmentName,
                        controller: this  // Asegura que el controlador sea pasado al fragmento
                    }).then(function (oFragment) {
                        return oFragment;
                    }).catch(function (error) {
                        console.error("Error loading fragment: ", error);
                    });
                }

                return this._formFragments[sFragmentName];
            },


            _showFormFragment: function (sFragmentName, existing) {
                var oPage = this.byId("dynamicPageId");

                // Load the fragment
                this._getFormFragment(sFragmentName).then(function (oVBox) {
                    if (!oVBox) {
                        console.error("Fragment could not be loaded.");
                        return;
                    }

                    // Check if the DynamicPage is still valid
                    if (!oPage.bIsDestroyed) {
                        // Set the fragment as content of the DynamicPage
                        oPage.setContent(oVBox);

                        var oSecondForm = oVBox.getItems()[1];
                        if (oSecondForm) {
                            var aSelects = oSecondForm.getContent().filter(function (oItem) {
                                return oItem instanceof sap.m.Select;
                            });

                            aSelects.forEach(function (oSelect) {
                                oSelect.setEditable(!existing);
                            });
                        } else {
                            console.error("Second form not found.");
                        }
                        oPage.bIsDestroyed = null;
                    } else {
                        // Handle the case where the DynamicPage has been destroyed
                        console.error("DynamicPage has been destroyed, cannot set content.");
                    }
                }).catch(function (error) {
                    // Handle errors loading the fragment
                    console.error("Error loading form fragment:", error);
                });
            },

            onCountryChange: function (oEvent) {
                var sSelectedKey = oEvent.getParameter("selectedItem").getKey();
                var oRutInput = this.byId("rutInput");
            
                if (sSelectedKey === "PE") {
                    oRutInput.setPlaceholder("RUC");
                    oRutInput.setMaxLength(11);
                } else if (sSelectedKey === "CL") {
                    oRutInput.setPlaceholder("RUT");
                    oRutInput.setMaxLength(10);
                } else {
                    oRutInput.setPlaceholder("N.I.F.");
                }
            
                oRutInput.setValue("");
            
                this.byId('overFlowTBar').setVisible(false);
            
                // Actualizar el fragmento con los datos del país seleccionado sin borrarlo
                this._showFormFragment("ClientDisplay", false);
            },
            

            clearPageData: function () {
                var oPage = this.byId("dynamicPageId");

                // Destroy existing content
                oPage.destroyContent();
            }
        });
    });
