/* global QUnit */

sap.ui.require(["atriacorps4hanape/recmasiva/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
