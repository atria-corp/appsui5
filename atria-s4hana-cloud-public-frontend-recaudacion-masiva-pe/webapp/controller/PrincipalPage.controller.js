sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/m/library",
    "sap/m/Button",
    "sap/ui/Device",
    "sap/ushell/Container",
    "sap/ui/core/format/DateFormat",
    "sap/ui/core/date/UI5Date",
    "sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel, MessageToast, Fragment, library, Button, Device, Container, DateFormat, UI5Date, Filter, FilterOperator) {
        "use strict";

        var URLHelper = library.URLHelper;
        var oComponent = null;
        var sDomain = "";
        var sDestination = "s4hana-extensibility/";
        var sPrefixOdata = "";
        var oView;
        return Controller.extend("atriacorps4hanape.recmasiva.controller.PrincipalPage", {
            onInit: function () {
                oComponent = this.getOwnerComponent();

                sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
                sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
                sDomain = sDomain.length > 0 ? sDomain : "./";

                this.oRouter = this.getOwnerComponent().getRouter()

                this._applicationsSwitchData()

                this._initApplicationPopover()

                this._initConfigurationDialogs()

                this._defineCustomModel()

                oView = this.getView();
                //var oJSONModel = this.initSampleDataModel();
                //console.log(oJSONModel)
                oView.setModel(this.initSampleDataModel(), "tmp");

                oView.setModel(new JSONModel({
                    globalFilter: "",
                    availabilityFilterOn: false,
                    cellFilterOn: false
                }), "ui");

                this._oGlobalFilter = null;
                this._oPriceFilter = null;

            },
            _filter: function() {
                var oFilter = null;
    
                if (this._oGlobalFilter && this._oPriceFilter) {
                    oFilter = new Filter([this._oGlobalFilter, this._oPriceFilter], true);
                } else if (this._oGlobalFilter) {
                    oFilter = this._oGlobalFilter;
                } else if (this._oPriceFilter) {
                    oFilter = this._oPriceFilter;
                }
    
                this.byId("table").getBinding().filter(oFilter, "Application");
            },
    
            filterGlobally: function(oEvent) {
                var sQuery = oEvent.getParameter("query");
                this._oGlobalFilter = null;
    
                if (sQuery) {
                    this._oGlobalFilter = new Filter([
                        new Filter("Name", FilterOperator.Contains, sQuery),
                        new Filter("Category", FilterOperator.Contains, sQuery)
                    ], false);
                }
    
                this._filter();
            },
    
            filterPrice: function(oEvent) {
                var oColumn = oEvent.getParameter("column");
                if (oColumn != this.byId("price")) {
                    return;
                }
    
                oEvent.preventDefault();
    
                var sValue = oEvent.getParameter("value");
    
                function clear() {
                    this._oPriceFilter = null;
                    oColumn.setFiltered(false);
                    this._filter();
                }
    
                if (!sValue) {
                    clear.apply(this);
                    return;
                }
    
                var fValue = null;
                try {
                    fValue = parseFloat(sValue, 10);
                } catch (e) {
                    // nothing
                }
    
                if (!isNaN(fValue)) {
                    this._oPriceFilter = new Filter("Price", FilterOperator.BT, fValue - 20, fValue + 20);
                    oColumn.setFiltered(true);
                    this._filter();
                } else {
                    clear.apply(this);
                }
            },
    
            clearAllFilters: function(oEvent) {
                var oTable = this.byId("table");
    
                var oUiModel = this.getView().getModel("ui");
                oUiModel.setProperty("/globalFilter", "");
                oUiModel.setProperty("/availabilityFilterOn", false);
    
                this._oGlobalFilter = null;
                this._oPriceFilter = null;
                this._filter();
    
                var aColumns = oTable.getColumns();
                for (var i = 0; i < aColumns.length; i++) {
                    oTable.filter(aColumns[i], null);
                }
            },
            initSampleDataModel: function() {
                var oModel = new JSONModel();
    
                var oDateFormat = DateFormat.getDateInstance({source: {pattern: "timestamp"}, pattern: "dd/MM/yyyy"});
    
                jQuery.ajax("https://sapui5.hana.ondemand.com/test-resources/sap/ui/documentation/sdk/products.json", {
                    dataType: "json",
                    success: function(oData) {
                        var aTemp1 = [];
                        var aTemp2 = [];
                        var aSuppliersData = [];
                        var aCategoryData = [];
                        for (var i = 0; i < oData.ProductCollection.length; i++) {
                            var oProduct = oData.ProductCollection[i];
                            if (oProduct.SupplierName && aTemp1.indexOf(oProduct.SupplierName) < 0) {
                                aTemp1.push(oProduct.SupplierName);
                                aSuppliersData.push({Name: oProduct.SupplierName});
                            }
                            if (oProduct.Category && aTemp2.indexOf(oProduct.Category) < 0) {
                                aTemp2.push(oProduct.Category);
                                aCategoryData.push({Name: oProduct.Category});
                            }
                            oProduct.DeliveryDate = Date.now() - (i % 10 * 4 * 24 * 60 * 60 * 1000);
                            oProduct.DeliveryDateStr = oDateFormat.format(UI5Date.getInstance(oProduct.DeliveryDate));
                            oProduct.Heavy = oProduct.WeightMeasure > 1000 ? "true" : "false";
                            oProduct.Available = oProduct.Status == "Available" ? true : false;
                        }
    
                        oData.Suppliers = aSuppliersData;
                        oData.Categories = aCategoryData;
    
                        oModel.setData(oData);
                    },
                    error: function() {
                        Log.error("failed to load json");
                    }
                });
    
                return oModel;
            },
            onInitialiseTable: function () {

                var smarTableComponent = this.getView().byId("tbl-documentos");

                var tableColumns = smarTableComponent.getTable().getColumns()

                tableColumns.forEach(oColumn => {

                    var oContext = this._configureTableColumn(oColumn.getFilterProperty());

                    oColumn.setHAlign(oContext.hAlign);
                    oColumn.setWidth(oContext.width);
                    oColumn.getLabel().setTextAlign("Center");
                    oColumn.getLabel().setWrapping(true);
                    oColumn.getLabel().setWrappingType("Hyphenated");
                    oColumn.mAggregations.customData[0].mProperties.value.type = oContext.type;
                    oColumn.mAggregations.customData[0].mProperties.value.width = null;
                    oColumn.setTemplate(oContext.control);

                })

            },

            _defineCustomModel: function () {

                debugger

                let oView = this.getView()

                let oTable = oView.byId("tbl-documentos")
                let oFilter = oView.byId("smartFilterBar")
                let oDataModel = this.getOwnerComponent().getModel("odata")

                oTable.setModel(oDataModel)
                oFilter.setModel(oDataModel)

            },

            _configureTableColumn: function (columnName) {
                var context = this;

                var oColumnsProperties = [
                    {
                        hAlign: "Center",
                        width: "12rem",
                        name: "NumeroDocumento",
                        control: new sap.m.Link({
                            text: "{= ${NumeroDocumento} }"
                        }).attachPress(function (evt) {
                            context.onListItemPress(evt);
                        })
                    }, {
                        hAlign: "Center",
                        width: "9rem",
                        name: "RutCliente",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${RutCliente} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "TipoDte",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${TipoDte} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "6rem",
                        name: "Estado",
                        type: "string",
                        control: new sap.m.ObjectStatus({
                            text: "{= ${Estado} }",
                            state: {
                                path: 'Estado',
                                formatter: function (estadoValue) {
                                    if (estadoValue === 'Aprobado') {
                                        return sap.ui.core.ValueState.Success;
                                    } else if (estadoValue === 'Cerrado') {
                                        return sap.ui.core.ValueState.Information;
                                    } else {
                                        // Handle other cases if needed
                                        return sap.ui.core.ValueState.Warning;
                                    }
                                }
                            },
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "FechaEmision",
                        type: "string",
                        control: new sap.m.Text({
                            //text: "{path: 'FechaEmision', type: 'sap.ui.model.type.DateTime', formatOptions: {UTC: false, pattern: 'dd.MM.yyyy'}}",
                            text: "{= ${FechaEmision} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "FechaVencimiento",
                        type: "string",
                        control: new sap.m.Text({
                            //text: "{path: 'FechaVencimiento', type: 'sap.ui.model.type.DateTime', formatOptions: {UTC: false, pattern: 'dd.MM.yyyy'}}",
                            text: "{= ${FechaVencimiento} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "6rem",
                        name: "IndicadorServicio",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${IndicadorServicio} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "9rem",
                        name: "TipoTransaccionVenta",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${TipoTransaccionVenta} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "6rem",
                        name: "MedioPago",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${MedioPago} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "FormaPago",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${FormaPago} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "GrupoRegistroCliente",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${GrupoRegistroCliente} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "MesServicio",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${MesServicio} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "MesFacturacion",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${MesFacturacion} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "GlosaPrincipal",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${GlosaPrincipal} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "12rem",
                        name: "TextoRegistro",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${TextoRegistro} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "9rem",
                        name: "NumeroContrato",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${NumeroContrato} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "NumeroDocumentoERP",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${NumeroDocumentoERP} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "10rem",
                        name: "TipoDocumentoErp",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${TipoDocumentoErp} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "8rem",
                        name: "ProcesadoPor",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${ProcesadoPor} }",
                            wrapping: false
                        })
                    }, {
                        hAlign: "Center",
                        width: "9rem",
                        name: "FechaHoraProceso",
                        type: "string",
                        control: new sap.m.Text({
                            text: "{= ${FechaHoraProceso} }",
                            wrapping: false
                        })
                    }
                ]

                var aColumn = $.grep(oColumnsProperties, function (item) {
                    if (columnName == item.name)
                        return item;
                });

                return aColumn.length > 0 ? aColumn[0] : {};

            },

            onPressVisualizarListado: function () {

                var view = this.getView()
                var model = view.getModel("customConfigurationModel")
                var data = model.getData()

                data.globalCurrentView = 'Listado'

                model.setData(data)

            },
            onPressVisualizarResumen: function () {

                var view = this.getView()
                var model = view.getModel("customConfigurationModel")
                var data = model.getData()

                data.globalCurrentView = 'Reporte'

                model.setData(data)

            },

            _initBusyIndicator: () => {

                sap.ui.core.BusyIndicator.show(0)

            },

            _hideBusyIndicator: () => {

                sap.ui.core.BusyIndicator.hide()

            },

            onListItemPress: function (oEvent) {

                var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(1)
                var currentDocument = oEvent.getSource().getParent().getRowBindingContext().getProperty()
                console.log(currentDocument)

                this.oRouter.navTo("detail", { layout: oNextUIState.layout, supplier: currentDocument.NumeroDocumento })
                
            },

            downloadExcelTemplate: async function () {
                this._initBusyIndicator()
                //var endpoint = "/s4hana-extensibility/sales/v1/sales-document/download-template"
                var endpoint = `${sDomain+sDestination}sales/v1/sales-document/download-template`;
                let response = { }

                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                };

                try {
                    let request = await fetch(endpoint, requestOptions)
                    if (request.ok) {
                        let blob = await request.blob();
                        const url = window.URL.createObjectURL(blob);
                        const a = document.createElement('a');
                        a.href = url;
                        a.download = 'Plantilla - Doc Ventas.xlsx'; // You can specify the filename here
                        a.style.display = 'none'; // Hide the element
                        document.body.appendChild(a);
                        a.click();
                        window.URL.revokeObjectURL(url);
                    } else {
                        throw new Error('Network response was not ok.');
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response;
            },

            readDataExcel: async function (oEvent) {
                //alert("Llamar al servicio para la lectura del excel")
                let oContext = this;
                console.log(oEvent)
                var oFiles = oEvent.getParameter("files")
                console.log(oFiles)

                var oFile = oFiles[0]
                console.log(oFile)

                const formdata = new FormData();
                formdata.append("file", oFile, oFile.name);
                var response = await this.fetchUploadExcel(formdata)
                oContext._showMessageViewModel( response.messages )
                if(response.success)
                    oContext.reloadModel();
                
                oContext._openDialogByName("PreviewData");
                
                console.log(response)
                
            },
            fetchUploadExcel: async function (formdata) {
                    
                //var endpoint = "s4hana-extensibility/sales/v1/sales-document/upload-massive-data";
                var endpoint = `${sDomain+sDestination}sales/v1/sales-document/upload-massive-data`;

                const requestOptions = {
                    method: "POST",
                    body: formdata,
                    redirect: "follow"
                };
                
                let response = { }

                try {
                    let request = await fetch(endpoint, requestOptions)
                    console.log(request)
                    
                    response = {
                        success: request.ok,
                        messages: [{
                            title: "Operación exitosa",
                            type: "Success",
                            description: "Se cargaron correctamente los documentos de venta",
                            subtitle: "Carga de documentos exitosa"
                        }],
                        response: {}
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response
            },

            deleteDocuments: async function (oEvent) {
                //alert("Llamar al servicio para la eliminación de información")
                var oContext = this;

                var oSmartTable = oContext.getView().byId("tbl-documentos");
                console.log(oSmartTable)
                var oTable = oSmartTable.getTable();
                console.log(oTable)
                var aSelectedItems = [];
                if (oTable instanceof sap.m.Table) {
                    aSelectedItems = oTable.getSelectedItems();
                } else if (oTable instanceof sap.ui.table.Table) {
                    aSelectedItems = oTable.getSelectedIndices().map(function(index) {
                        return oTable.getContextByIndex(index).getObject();
                    });
                } else {
                    // Handle other types of tables
                    console.error("Unsupported table type");
                }
                let idArray = []
                if(aSelectedItems.length>0){
                    console.log("Selected " + aSelectedItems.length + " document(s)", aSelectedItems)
                    let message  = `¿Está seguro que desea eliminar ${aSelectedItems.length} registro(s)?`
                    let title    = 'Confirmación'
                    let actions  = [ "Si", "No" ]
                    let onPress  = async function( option ) {

                        switch( option ){

                            case "Si" : 
                            aSelectedItems.forEach(function (item) {
                                console.log(item);
                                let idUrl = item.__metadata.id;
                                console.log(idUrl)
                                let sapId = idUrl.match(/'([^']+)'/)[1];
                                console.log(sapId)
                                idArray.push(sapId)
                            });
                            console.log("TestingArray",idArray)
                            let requestResponse = await oContext.fetchDeleteDocument( idArray );
                            oContext._showMessageViewModel( requestResponse.messages )
                            console.log( requestResponse.messages )

                            oContext.reloadModel()
                            break;
                        }
                    }

                    sap.m.MessageBox.confirm( message, {
                        title,
                        actions,
                        emphasizedAction: sap.m.MessageBox.Action.YES,
                        onClose: onPress
                    } );
                    
                }else
                    MessageToast.show("Seleccione los documentos");
            },
            
            fetchDeleteDocument: async function (idArray) {
                this._initBusyIndicator();
                var endpoint = "s4hana-extensibility/sales/v1/sales-document/delete-sales-document";
                console.log(endpoint)
                let response = { }

                const requestBody = idArray;
                

                var requestOptions = {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(requestBody)
                };

                try {
                    let request = await fetch(endpoint, requestOptions)
                    //let responseJson = await request.json()
                    //console.log("TestingDelete", responseJson)
                    if(request.status === 200){
                        response = {
                            success: true,
                            messages: [{
                                title: "Operación exitosa",
                                type: "Success",
                                description: `Se eliminó correctamente el/los ${requestBody.length} documento(s)`,
                                subtitle: "Eliminación de documentos exitosa."
                            }],
                            response: {}
                        }
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response
            },
            
            reloadModel: function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("odata")
                if(oModel){
                    oModel.refresh()
                    oModel.read("/DocumentoVentaSigges", {
                        success: function() {
                            // Data read successfully, no action required
                        },
                        error: function() {
                            console.error("Failed to reload model");
                        }
                    });
                } else {
                    console.error("Model not found");
                }
            },_showMessageViewModel: function (messages) {
                var oContext = this;

                var oBackButton = new sap.m.Button({
                    icon: "sap-icon://nav-back",
                    visible: false,
                    press: function () {
                        oContext.oMessageView.navigateBack();
                        this.setVisible(true);
                    }
                });
                    
                var oMessageTemplate = new sap.m.MessageItem({
                    type: '{MessageViewModel>type}',
                    title: '{MessageViewModel>title}',
                    description: '{MessageViewModel>description}',
                    subtitle: '{MessageViewModel>subtitle}',
                    counter: '{MessageViewModel>counter}',
                });
        
        
                this.oMessageView = new sap.m.MessageView({
                    showDetailsPageHeader: true,
                    itemSelect: function () {
                        oBackButton.setVisible(true);
                    },
                    items: {
                        path: "MessageViewModel>/",
                        template: oMessageTemplate
                    }
                });
        
                this.oDialog = new sap.m.Dialog({
                    title: "Log de Transacción",
                    resizable: true,
                    content: this.oMessageView,
                    beginButton: new Button({
                        press: function () {
                        this.getParent().close();
                        },
                        text: "Close"
                    }),
                    contentHeight: "50%",
                    contentWidth: "50%",
                    verticalScrolling: false
                });
        
                var oModel = new JSONModel();
            
                oModel.setData( messages );

                this.oMessageView.setModel( oModel, "MessageViewModel" );
                
                this.oMessageView.navigateBack();
                
                this.oDialog.open();

            },sendToErp: function (oEvent) {
                //alert( "Llamar al servicio ODATA para la creación de pedido de venta" )
                
                var oSmartTable = this.getView().byId("tbl-documentos");
                console.log(oSmartTable)
                var oTable = oSmartTable.getTable();
                console.log(oTable)
                var aSelectedItems = [];
                if (oTable instanceof sap.m.Table) {
                    aSelectedItems = oTable.getSelectedItems();
                } else if (oTable instanceof sap.ui.table.Table) {
                    aSelectedItems = oTable.getSelectedIndices().map(function(index) {
                        return oTable.getContextByIndex(index).getObject();
                    });
                } else {
                    console.error("Unsupported table type");
                }

                if(aSelectedItems.length>0){
                    console.log("Selected " + aSelectedItems.length + " document(s)")
                }else
                    MessageToast.show("Seleccione los documentos");
                

            },

            _setDialogConfig: async function (name, path) {

                let oContext = this

                let dialogs = oContext.customDialogs

                if (!dialogs) {

                    oContext.customDialogs = []
                    dialogs = oContext.customDialogs

                }

                let dialog = oContext.loadFragment({ name: path })

                dialogs.push({
                    name,
                    path,
                    dialog
                })

            },

            _getDialogConfig: function (name) {

                let oContext = this

                let dialogs = oContext.customDialogs ? oContext.customDialogs : []

                return dialogs.find((item) => item.name === name)

            },

            _initConfigurationDialogs: async function () {

                let oContext = this

                await oContext._setDialogConfig("ImportData", "atriacorps4hanape.recmasiva.view.fragment.ImportData")
                await oContext._setDialogConfig("PreviewData", "atriacorps4hanape.recmasiva.view.fragment.PreviewData")

            },

            _openDialogByName: async function (inputName) {

                let oContext = this

                let dialogConfig = oContext._getDialogConfig(inputName)

                dialogConfig.dialog.then((oDialog) => oDialog.open())

            },

            _closeDialogByName: async function (inputName) {

                let oContext = this

                let dialogConfig = oContext._getDialogConfig(inputName)

                dialogConfig.dialog.then((oDialog) => oDialog.close())


            },

            _applicationsSwitchData: function () {

                let applications = {
                    "items": [
                        {
                            "src": "sap-icon://sap-logo-shape",
                            "title": "SAP Homepage",
                            "subTitle": "Learn more about SAP",
                            "targetSrc": "https://www.sap.com/index.html",
                            "target": "_blank"
                        },
                        {
                            "src": "sap-icon://group",
                            "title": "Clientes",
                            "subTitle": "Consulta/Creacion",
                            "targetSrc": "https://community.sap.com/topics/ui5",
                            "target": "_blank"
                        }
                    ]
                }

                let oView = this.getView()

                let oModel = new JSONModel(applications)

                oView.setModel(oModel, "applicationsModel")

            },

            _initApplicationPopover: function () {

                let oView = this.getView()

                if (!this._pPopover) {
                    this._pPopover = Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hanape.recmasiva.view.fragment.ApplicationSwitch",
                        controller: this
                    }).then(function (oPopover) {
                        oView.addDependent(oPopover);
                        if (Device.system.phone) {
                            oPopover.setEndButton(new Button({ text: "Close", type: "Emphasized", press: this.fnClose.bind(this) }));
                        }
                        return oPopover;
                    }.bind(this));
                }

            },

            fnChange: function (oEvent) {
                var oItemPressed = oEvent.getParameter("itemPressed"),
                    sTargetSrc = oItemPressed.getTargetSrc();

                MessageToast.show("Redireccionando... " + sTargetSrc);

                //URLHelper.redirect(sTargetSrc, true);


                sap.ui.require(["sap/ushell/Container"], async (Container) => {
                    const oNavigationService = await Container.getServiceAsync("Navigation");
                    const sHref = await oNavigationService.getHref({
                      target : {
                        semanticObject: "atriacorps4hanachmantbp",
                        action: "Display"
                      }
                    }, oComponent);
                    console.log(sHref)
                    URLHelper.redirect(sHref, false);
                    // do something with the resolved sHref.
                  });



            },
            fnOpen: function (oEvent) {
                var oButton = this.getView().byId("pSwitchBtn");
                this._pPopover.then(function (oPopover) {
                    oPopover.openBy(oButton);
                });
            },
            fnClose: function () {
                this._pPopover.then(function (oPopover) {
                    oPopover.close();
                });
            }

        });
    });

