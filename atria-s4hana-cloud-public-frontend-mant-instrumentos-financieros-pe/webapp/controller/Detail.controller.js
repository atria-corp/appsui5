sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox"
], function (JSONModel, Controller, MessageBox) {
    "use strict";

    var globalCurrentDocument
	var contextGlobal

    return Controller.extend("atriacorps4hanape.mantinstfnc.controller.Detail", {
        onInit: async function () {

            contextGlobal = this

            var oExitButton = this.getView().byId("exitFullScreenBtn")
            var oEnterButton = this.getView().byId("enterFullScreenBtn")

            this.oRouter = this.getOwnerComponent().getRouter()
            this.oModel = this.getOwnerComponent().getModel()

            this.oRouter.getRoute("detail").attachPatternMatched(this._bindingDocumentMatcher, this);

            [oExitButton, oEnterButton].forEach(function (oButton) {
                oButton.addEventDelegate({
                    onAfterRendering: function () {
                        if (this.bFocusFullScreenButton) {
                            this.bFocusFullScreenButton = false;
                            oButton.focus();
                        }
                    }.bind(this)
                });
            }, this);

        },

        handleFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.NumeroDocumento });
        },
        handleExitFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.NumeroDocumento });
        },
        handleClose: function () {
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
            this.oRouter.navTo("list", { layout: sNextLayout });
        },_bindingDocumentMatcher: async function (oEvent) {
            
            //--- Obtenemos la view
			let oView = this.getView()

			//--- Obtenemos la posición dentro del array del proveedor seleccionado
			const NumeroDocumento = oEvent.getParameter("arguments").supplier || 0

            console.log("Testing", oView.getModel("odata"))
            console.log("Testing2", oView.getModel("odata").oData)

            const oData = oView.getModel("odata").oData

            globalCurrentDocument = Object.values(oData).find( (item) => item.NumeroDocumento == NumeroDocumento )
                        
            let documentoId = globalCurrentDocument.NumeroDocumento;
            let modelDocumentData = new JSONModel( globalCurrentDocument )
			
            oView.setModel( modelDocumentData, "documentData" )

            debugger

			var documentDetails = await this._fetchDocumentDetails(documentoId)
            
			var documentReferences = await this._fetchDocumentReference(documentoId)
            
            var documentLogs = await this._fetchDocumentLogs(documentoId)
            
			let detailModel = new JSONModel(documentDetails.d.results)
			oView.setModel(detailModel, "documentsDetails")
			//detailModel.attachRequestCompleted(processFlowComponent.updateModel.bind(processFlowComponent))

            let referenceModel = new JSONModel(documentReferences.d.results)
			oView.setModel(referenceModel, "documentsReference")

            let logModel = new JSONModel(documentLogs.d.results)
			oView.setModel(logModel, "documentsLogs")

		},_fetchDocumentDetails: async (sapId) => {
			contextGlobal._initBusyIndicator()
            
            var endpoint = "/s4hana-extensibility/odata.svc/DetalleVentaSigges?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            
			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				let responseJson = await request.json()
                console.log("TestingResult", responseJson)
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},_fetchDocumentReference: async (sapId) => {
			contextGlobal._initBusyIndicator()
			var endpoint = "/s4hana-extensibility/odata.svc/Referencias?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            let responseJson;

			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				responseJson = await request.json()
                console.log("TestingResult", responseJson)
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},_fetchDocumentLogs: async (sapId) => {
			contextGlobal._initBusyIndicator()
            console.log("TestingInside")
			var endpoint = "/s4hana-extensibility/odata.svc/LogTransaccions?$format=json&$filter=NumeroDocumento eq '"+sapId+"'";
            
			var myHeaders = new Headers({
                "Access-Control-Allow-Origin" : "*",
              });
			
			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
                redirect: 'follow'
			};
            console.log("TestingEndpoint", endpoint)
			try {
				let request = await fetch(endpoint, requestOptions)
				let responseJson = await request.json()
                console.log("TestingResult", responseJson)
				return responseJson
			} catch (ex) {
				console.error("TestingError", ex)
			} finally {
                console.log("TestingEnd")
				contextGlobal._hideBusyIndicator()
			}
		},_buildProcessFlowInput: (detailList) => {
			let position = 0
			let listSize = detailList.length
            console.log("TestingSize",listSize)
			let lanes = []
			let nodes = []

			detailList.forEach(item => {
                console.log("TestingItem",item)
				

			})
			return { "lanes": lanes, "nodes": nodes }
		},_initBusyIndicator: () => {
			sap.ui.core.BusyIndicator.show()
		},_hideBusyIndicator: () => {
			sap.ui.core.BusyIndicator.hide()
		},
    });
});