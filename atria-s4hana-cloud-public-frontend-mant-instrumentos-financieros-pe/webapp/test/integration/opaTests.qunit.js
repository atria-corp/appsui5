/* global QUnit */

sap.ui.require(["atriacorps4hanape/mantinstfnc/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
