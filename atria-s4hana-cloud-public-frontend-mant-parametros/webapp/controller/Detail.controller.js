sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    'sap/m/Bar',
    'sap/m/Title',
    "sap/m/Popover",
    "sap/m/Dialog"
], function (JSONModel, Controller, MessageToast, MessageBox, Fragment, Bar, Title, Popover, Dialog) {
    "use strict";

    var globalCurrentDocument
    var contextGlobal
    var sDomain = "";
    var sDestination = "s4hana-extensibility/";
    var sPrefixOdata = "";
    return Controller.extend("atriacorps4hana.mantparam.controller.Detail", {

        onInit: async function () {

            contextGlobal = this

            var oExitButton = this.getView().byId("exitFullScreenBtn")
            var oEnterButton = this.getView().byId("enterFullScreenBtn")

            this.oRouter = this.getOwnerComponent().getRouter()
            this.oModel = this.getOwnerComponent().getModel()
            sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
            sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
            sDomain = sDomain.length > 0 ? sDomain : "./";
            this.oRouter.getRoute("detail").attachPatternMatched(this._bindingDocumentMatcher, this);

            [oExitButton, oEnterButton].forEach(function (oButton) {
                oButton.addEventDelegate({
                    onAfterRendering: function () {
                        if (this.bFocusFullScreenButton) {
                            this.bFocusFullScreenButton = false;
                            oButton.focus();
                        }
                    }.bind(this)
                });
            }, this);

        },

        handleFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.GlobalSettingId });
        },
        handleExitFullScreen: function () {
            this.bFocusFullScreenButton = true;
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
            this.oRouter.navTo("detail", { layout: sNextLayout, supplier: globalCurrentDocument.GlobalSettingId });
        },
        handleClose: function () {
            var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
            this.oRouter.navTo("list", { layout: sNextLayout });
        },

        _bindingDocumentMatcher: async function (oEvent) {

            //--- Obtenemos la view
            let oView = this.getView()

            debugger

            //--- Obtenemos la posición dentro del array del proveedor seleccionado
            const Identification = oEvent.getParameter("arguments").supplier || 0

            console.log("Testing", oView.getModel("odata"))
            console.log("Testing2", oView.getModel("odata").oData)

            const oData = oView.getModel("odata").oData

            globalCurrentDocument = Object.values(oData).find((item) => item.GlobalSettingId == Identification)

            let patternId = globalCurrentDocument.GlobalSettingId
            let modelDocumentData = new JSONModel(globalCurrentDocument)

            oView.setModel(modelDocumentData, "documentData")

            debugger

            this._updateDocumentDetailsData(patternId)
        },

        _updateDocumentDetailsData: async function(patternId){
            var documentDetails = await this._fetchDocumentDetails(patternId)
            let detailModel = new JSONModel(documentDetails.d.results)
            this.getView().setModel(detailModel, "documentsDetails")
        },

        _fetchDocumentDetails: async (patternId) => {

            contextGlobal._initBusyIndicator()

            //var endpoint = "/s4hana-extensibility/odata.svc/GlobalSettings?$format=json&$filter=GlobalSettingPatternId eq " + patternId + "";
            var endpoint = `${sDomain+sDestination}odata.svc/GlobalSettings?$format=json&$filter=GlobalSettingPatternId eq ${patternId}`;

            var myHeaders = new Headers({
                "Access-Control-Allow-Origin": "*",
            });

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };
            console.log("TestingEndpoint", endpoint)
            try {
                let request = await fetch(endpoint, requestOptions)
                let responseJson = await request.json()
                console.log("TestingResult", responseJson)
                return responseJson
            } catch (ex) {
                console.error("TestingError", ex)
            } finally {
                console.log("TestingEnd")
                contextGlobal._hideBusyIndicator()
            }
        },
        _buildProcessFlowInput: (detailList) => {
            let position = 0
            let listSize = detailList.length
            console.log("TestingSize", listSize)
            let lanes = []
            let nodes = []

            detailList.forEach(item => {
                console.log("TestingItem", item)


            })
            return { "lanes": lanes, "nodes": nodes }
        }, _initBusyIndicator: () => {
            sap.ui.core.BusyIndicator.show()
        }, _hideBusyIndicator: () => {
            sap.ui.core.BusyIndicator.hide()
        },
        loadCreateForm: function (patternId, module, type) {
            var oView = this.getView();
            let oContext = this;
            let newModel = new JSONModel();

            if (!newModel.getProperty("/GlobalSettingClassification")) {
                newModel.setProperty("/GlobalSettingClassification", "CHILDREN");
            }
            if (!newModel.getProperty("/GlobalSettingStatus")) {
                newModel.setProperty("/GlobalSettingStatus", "ACTIVO");
            }
            if (!newModel.getProperty("/GlobalSettingPatternId")) {
                newModel.setProperty("/GlobalSettingPatternId", patternId);
            }
            if (!newModel.getProperty("/GlobalSettingType")) {
                newModel.setProperty("/GlobalSettingType", type);
            }
            if (!newModel.getProperty("/GlobalSettingModule")) {
                newModel.setProperty("/GlobalSettingModule", module);
            }
            oContext.getView().setModel(newModel, "newGlobalSetting");

            if (this._oFormDialog) {
                this._oFormDialog.close();
                this._oFormDialog.destroy();
                this._oFormDialog = null;
            }

            var oDialogBar = new Bar({
                contentMiddle: [
                    new Title({ text: "Crear Children" })
                ]
            });

            this._oFormDialog = new Dialog({
                customHeader: oDialogBar,
                contentWidth: "40%",
                verticalScrolling: false,
                afterClose: function () {
                    newModel.setData({});
                }
            });

            Fragment.load({
                id: oView.getId(),
                name: "atriacorps4hana.mantparam.view.fragment.FormApp",
                controller: this
            }).then(function (oFragment) {
                oView.addDependent(oFragment);
                this._oFormDialog.addContent(oFragment);
                this._oFormDialog.open();
            }.bind(this));
            this._isCreateOperation = true;
        },

        loadEditForm: function (oItemData) {
            var oView = this.getView();
            let oContext = this;
            let newModel = new JSONModel();

            newModel.setData(oItemData);

            oContext.getView().setModel(newModel, "newGlobalSetting");

            if (this._oFormDialog) {
                this._oFormDialog.close();
                this._oFormDialog.destroy();
                this._oFormDialog = null;
            }

            var oDialogBar = new Bar({
                contentMiddle: [
                    new Title({ text: "Editar Children" })
                ]
            });

            this._oFormDialog = new Dialog({
                customHeader: oDialogBar,
                contentWidth: "40%",
                verticalScrolling: false,
                afterClose: function () {
                    newModel.setData({});
                }
            });

            Fragment.load({
                id: oView.getId(),
                name: "atriacorps4hana.mantparam.view.fragment.FormApp",
                controller: this
            }).then(function (oFragment) {
                oView.addDependent(oFragment);
                this._oFormDialog.addContent(oFragment);
                this._oFormDialog.open();
            }.bind(this));
            this._isCreateOperation = false;

        },
        onEditButtonClick: function (oEvent) {
            var oItemContext = oEvent.getSource().getBindingContext("documentsDetails");
            var oItemData = oItemContext.getObject();
            this.loadEditForm(oItemData);
        },

        onCreateButtonClick: function () {
            var oModel = this.getView().getModel("documentData");
            var patternId = oModel.getProperty("/GlobalSettingId");
            var module = oModel.getProperty("/GlobalSettingModule");
            var type = oModel.getProperty("/GlobalSettingType")
            this.loadCreateForm(patternId, module, type)
        },
        cancelCreate: function (oEvent) {
            this._oFormDialog.close();
        },
        saveGSonERP: async function (oEvent) {
            this._initBusyIndicator();
            var requestBody = this.getView().getModel("newGlobalSetting").getData();
            console.log(this.getView().getModel("newGlobalSetting"));

            //var endpoint = "/s4hana-extensibility/odata.svc/GlobalSettings";
            var endpoint = `${sDomain+sDestination}odata.svc/GlobalSettings`;
            requestBody.GlobalSettingPatternId = parseInt(requestBody.GlobalSettingPatternId, 10);

            var requestOptions = {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            };

            try {
                let request;
                if (this._isCreateOperation) {
                    requestOptions.method = 'POST';
                } else {
                    requestOptions.method = 'PUT';
                    endpoint += `(${requestBody.GlobalSettingId})`;
                }

                request = await fetch(endpoint, requestOptions);

                if (request.status === 200 || request.status === 201 || request.status === 204) {
                    MessageToast.show("Operación exitosa. El parámetro fue " + (this._isCreateOperation ? "creado" : "editado") + " correctamente.");
                    this._updateDocumentDetailsData(requestBody.GlobalSettingPatternId)
                } else {
                    MessageBox.error("Ocurrió un problema al " + (this._isCreateOperation ? "crear" : "editar") + " el parámetro. Por favor, inténtelo nuevamente.");
                }
            } catch (error) {
                MessageBox.error("Ocurrió un problema al comunicarse con el servidor. Por favor, inténtelo nuevamente.");
            } finally {
                this._hideBusyIndicator();
                if (this._oFormDialog) {
                    this._oFormDialog.close();
                }
            }
        },
        downloadExcelTemplate: async function () {
            let documentData = this.getView().getModel('documentData').getData()
            let documentModule = documentData.GlobalSettingModule
            let documentType = documentData.GlobalSettingType

            this._initBusyIndicator()
            //var endpoint = `/s4hana-extensibility/v1/global-setting/export-setting-by-module-and-type?Type=${documentType}&Module=${documentModule}`
            var endpoint = `${sDomain+sDestination}v1/global-setting/export-setting-by-module-and-type?Type=${documentType}&Module=${documentModule}`;
            let response = { }

            const requestOptions = {
                method: "GET",
                redirect: "follow"
            };

            try {
                let request = await fetch(endpoint, requestOptions)
                if (request.ok) {
                    let blob = await request.blob();
                    const url = window.URL.createObjectURL(blob);
                    const a = document.createElement('a');
                    a.href = url;
                    a.download = 'Export - Global Settings.xlsx'; // You can specify the filename here
                    a.style.display = 'none'; // Hide the element
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(url);
                } else {
                    throw new Error('Network response was not ok.');
                }
            } catch (error) {
                response = {
                    success: false,
                    messages: [{
                        title: "Ocurrió un problema",
                        type: "Error",
                        description: `${error}`,
                        subtitle: "Comuníquese con el administrador del sistema."
                    }],
                    response: {}
                }
            } finally {
                this._hideBusyIndicator()
            }
            return response;
        },

    });
});