sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/m/library",
    "sap/ui/Device",
    "sap/m/Button",
    'sap/m/Bar',
    'sap/m/Title',
    "sap/m/Popover",
    "sap/m/Dialog"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel, MessageToast, Fragment, library, Device, Button, Bar, Title, Popover, Dialog) {
        "use strict";

        var URLHelper = library.URLHelper;
        var sDomain = "";
        var sDestination = "s4hana-extensibility/";
        var sPrefixOdata = "";
        return Controller.extend("atriacorps4hana.mantparam.controller.PrincipalPage", {
            onInit: function () {

                this.oRouter = this.getOwnerComponent().getRouter()
                sPrefixOdata = this.getOwnerComponent().getModel("odata").sServiceUrl;
                sDomain = this.getOwnerComponent().getModel("odata").sServiceUrl.split("s4hana-extensibility")[0];
                sDomain = sDomain.length > 0 ? sDomain : "./";
                //sDomain = oOwnerComponent.getModel("ipharma")
                //console.log(sDomain+sDestination)
                //$.ajax("../081a3e16-3dd9-43bf-a9ac-f9436dfa7767.atriacorps4hanamantparam.atriacorps4hanamantparam/~220524223203+0000~/s4hana-extensibility/odata.svc/GlobalSettings")

                this._applicationsSwitchData()

                this._initApplicationPopover()

                this._initConfigurationDialogs()

                this._defineCustomModel()

                this.onInitialiseList()
            },

            onInitialiseList: function () {

                var smartListComponent = this.getView().byId("list-settings");
                console.log("Test1",smartListComponent)
                console.log("Test2",smartListComponent.getMetadata())

                var listElemTemplate = smartListComponent.getListItemTemplate()
                console.log("Test3",listElemTemplate)
                console.log("Test4",listElemTemplate.getMetadata())
                

            },

            _defineCustomModel: function () {

                debugger

                let oView = this.getView()

                let oList = oView.byId("list-settings")
                let oFilter = oView.byId("smartFilterBar")
                let oDataModel = this.getOwnerComponent().getModel("odata")
                console.log(oDataModel);
                
                oList.setModel(oDataModel)
                oFilter.setModel(oDataModel)

                //oList.getBinding("items").filter(new sap.ui.model.Filter("GlobalSettingClassification", sap.ui.model.FilterOperator.EQ, "PATTERN"));
            },

            onPressVisualizarListado: function () {

                var view = this.getView()
                var model = view.getModel("customConfigurationModel")
                var data = model.getData()

                data.globalCurrentView = 'Listado'

                model.setData(data)

            },
            onPressVisualizarResumen: function () {

                var view = this.getView()
                var model = view.getModel("customConfigurationModel")
                var data = model.getData()

                data.globalCurrentView = 'Reporte'

                model.setData(data)

            },

            _initBusyIndicator: () => {

                sap.ui.core.BusyIndicator.show(0)

            },

            _hideBusyIndicator: () => {

                sap.ui.core.BusyIndicator.hide()

            },

            onListItemPress: function (oEvent) {
                console.log("Entro", oEvent)
                var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(1)
                var currentDocument = oEvent.getSource().getBindingContext().getObject()
                console.log(currentDocument)

                this.oRouter.navTo("detail", { layout: oNextUIState.layout, supplier: currentDocument.GlobalSettingId })
                
            },

            downloadExcelTemplate: async function () {
                this._initBusyIndicator()
                
                //var endpoint = "/s4hana-extensibility/v1/global-setting/export-all-setting"
                var endpoint = `${sDomain+sDestination}v1/global-setting/export-all-setting`;
                let response = { }

                const requestOptions = {
                    method: "GET",
                    redirect: "follow"
                };

                try {
                    let request = await fetch(endpoint, requestOptions)
                    if (request.ok) {
                        let blob = await request.blob();
                        const url = window.URL.createObjectURL(blob);
                        const a = document.createElement('a');
                        a.href = url;
                        a.download = 'Export - Global Settings.xlsx'; // You can specify the filename here
                        a.style.display = 'none'; // Hide the element
                        document.body.appendChild(a);
                        a.click();
                        window.URL.revokeObjectURL(url);
                    } else {
                        throw new Error('Network response was not ok.');
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response;
            },

            readDataExcel: async function (oEvent) {
                //alert("Llamar al servicio para la lectura del excel")
                let oContext = this;
                console.log(oEvent)
                var oFiles = oEvent.getParameter("files")
                console.log(oFiles)

                var oFile = oFiles[0]
                console.log(oFile)

                const formdata = new FormData();
                formdata.append("file", oFile, oFile.name);
                var response = await this.fetchUploadExcel(formdata)
                oContext._showMessageViewModel( response.messages )
                if(response.success)
                    oContext.reloadModel();
                console.log(response)
                
            },
            fetchUploadExcel: async function (formdata) {
                
                //var endpoint = "s4hana-extensibility/v1/global-setting/import-global-setting";
                var endpoint = `${sDomain+sDestination}v1/global-setting/import-global-setting`;

                const requestOptions = {
                    method: "POST",
                    body: formdata,
                    redirect: "follow"
                };
                
                let response = { }

                try {
                    let request = await fetch(endpoint, requestOptions)
                    console.log(request)
                    
                    response = {
                        success: request.ok,
                        messages: [{
                            title: "Operación exitosa",
                            type: "Success",
                            description: "Se cargaron correctamente los parámetros",
                            subtitle: "Carga de global settings exitosa"
                        }],
                        response: {}
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response
            },

            deleteDocuments: async function (oEvent) {
                //alert("Llamar al servicio para la eliminación de información")
                var oContext = this;

                var oSmartTable = oContext.getView().byId("tbl-documentos");
                console.log(oSmartTable)
                var oTable = oSmartTable.getTable();
                console.log(oTable)
                var aSelectedItems = [];
                if (oTable instanceof sap.m.Table) {
                    aSelectedItems = oTable.getSelectedItems();
                } else if (oTable instanceof sap.ui.table.Table) {
                    aSelectedItems = oTable.getSelectedIndices().map(function(index) {
                        return oTable.getContextByIndex(index).getObject();
                    });
                } else {
                    // Handle other types of tables
                    console.error("Unsupported table type");
                }
                let idArray = []
                if(aSelectedItems.length>0){
                    console.log("Selected " + aSelectedItems.length + " document(s)", aSelectedItems)
                    let message  = `¿Está seguro que desea eliminar ${aSelectedItems.length} registro(s)?`
                    let title    = 'Confirmación'
                    let actions  = [ "Si", "No" ]
                    let onPress  = async function( option ) {

                        switch( option ){

                            case "Si" : 
                            aSelectedItems.forEach(function (item) {
                                console.log(item);
                                let idUrl = item.__metadata.id;
                                console.log(idUrl)
                                let sapId = idUrl.match(/'([^']+)'/)[1];
                                console.log(sapId)
                                idArray.push(sapId)
                            });
                            console.log("TestingArray",idArray)
                            let requestResponse = await oContext.fetchDeleteDocument( idArray );
                            oContext._showMessageViewModel( requestResponse.messages )
                            console.log( requestResponse.messages )

                            oContext.reloadModel()
                            break;
                        }
                    }

                    sap.m.MessageBox.confirm( message, {
                        title,
                        actions,
                        emphasizedAction: sap.m.MessageBox.Action.YES,
                        onClose: onPress
                    } );
                    
                }else
                    MessageToast.show("Seleccione los documentos");
            },
            
            refreshoData: function(){
                var view = this.getView()
                var model = view.getModel("odata")
                model.refresh()
            },

            fetchDeleteDocument: async function (idArray) {
                this._initBusyIndicator();
                var endpoint = "s4hana-extensibility/sales/v1/sales-document/delete-sales-document";
                console.log(endpoint)
                let response = { }

                const requestBody = idArray;
                

                var requestOptions = {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(requestBody)
                };

                try {
                    let request = await fetch(endpoint, requestOptions)
                    //let responseJson = await request.json()
                    //console.log("TestingDelete", responseJson)
                    if(request.status === 200){
                        response = {
                            success: true,
                            messages: [{
                                title: "Operación exitosa",
                                type: "Success",
                                description: `Se eliminó correctamente el/los ${requestBody.length} documento(s)`,
                                subtitle: "Eliminación de documentos exitosa."
                            }],
                            response: {}
                        }
                    }
                } catch (error) {
                    response = {
                        success: false,
                        messages: [{
                            title: "Ocurrió un problema",
                            type: "Error",
                            description: `${error}`,
                            subtitle: "Comuníquese con el administrador del sistema."
                        }],
                        response: {}
                    }
                } finally {
                    this._hideBusyIndicator()
                }
                return response
            },
            
            reloadModel: function () {
                let oContext = this;
                let oModel = oContext.getView().getModel("odata")
                if(oModel){
                    oModel.refresh()
                    oModel.read("/GlobalSettings", {
                        success: function() {
                            // Data read successfully, no action required
                        },
                        error: function() {
                            console.error("Failed to reload model");
                        }
                    });
                } else {
                    console.error("Model not found");
                }
            },_showMessageViewModel: function (messages) {
                var oContext = this;

                var oBackButton = new sap.m.Button({
                    icon: "sap-icon://nav-back",
                    visible: false,
                    press: function () {
                        oContext.oMessageView.navigateBack();
                        this.setVisible(true);
                    }
                });
                    
                var oMessageTemplate = new sap.m.MessageItem({
                    type: '{MessageViewModel>type}',
                    title: '{MessageViewModel>title}',
                    description: '{MessageViewModel>description}',
                    subtitle: '{MessageViewModel>subtitle}',
                    counter: '{MessageViewModel>counter}',
                });
        
        
                this.oMessageView = new sap.m.MessageView({
                    showDetailsPageHeader: true,
                    itemSelect: function () {
                        oBackButton.setVisible(true);
                    },
                    items: {
                        path: "MessageViewModel>/",
                        template: oMessageTemplate
                    }
                });
        
                this.oDialog = new sap.m.Dialog({
                    title: "Log de Transacción",
                    resizable: true,
                    content: this.oMessageView,
                    beginButton: new Button({
                        press: function () {
                        this.getParent().close();
                        },
                        text: "Close"
                    }),
                    contentHeight: "50%",
                    contentWidth: "50%",
                    verticalScrolling: false
                });
        
                var oModel = new JSONModel();
            
                oModel.setData( messages );

                this.oMessageView.setModel( oModel, "MessageViewModel" );
                
                this.oMessageView.navigateBack();
                
                this.oDialog.open();

            },sendToErp: function (oEvent) {
                //alert( "Llamar al servicio ODATA para la creación de pedido de venta" )
                
                var oSmartTable = this.getView().byId("tbl-documentos");
                console.log(oSmartTable)
                var oTable = oSmartTable.getTable();
                console.log(oTable)
                var aSelectedItems = [];
                if (oTable instanceof sap.m.Table) {
                    aSelectedItems = oTable.getSelectedItems();
                } else if (oTable instanceof sap.ui.table.Table) {
                    aSelectedItems = oTable.getSelectedIndices().map(function(index) {
                        return oTable.getContextByIndex(index).getObject();
                    });
                } else {
                    console.error("Unsupported table type");
                }

                if(aSelectedItems.length>0){
                    console.log("Selected " + aSelectedItems.length + " document(s)")
                }else
                    MessageToast.show("Seleccione los documentos");
                

            },

            _setDialogConfig: async function (name, path) {

                let oContext = this

                let dialogs = oContext.customDialogs

                if (!dialogs) {

                    oContext.customDialogs = []
                    dialogs = oContext.customDialogs

                }

                let dialog = oContext.loadFragment({ name: path })

                dialogs.push({
                    name,
                    path,
                    dialog
                })

            },

            _getDialogConfig: function (name) {

                let oContext = this

                let dialogs = oContext.customDialogs ? oContext.customDialogs : []

                return dialogs.find((item) => item.name === name)

            },

            _initConfigurationDialogs: async function () {

                let oContext = this

                await oContext._setDialogConfig("ImportData", "atriacorps4hana.mantparam.view.fragment.ImportData")

            },

            _openDialogByName: async function (inputName) {

                let oContext = this

                let dialogConfig = oContext._getDialogConfig(inputName)

                dialogConfig.dialog.then((oDialog) => oDialog.open())

            },

            _closeDialogByName: async function (inputName) {

                let oContext = this

                let dialogConfig = oContext._getDialogConfig(inputName)

                dialogConfig.dialog.then((oDialog) => oDialog.close())


            },

            _applicationsSwitchData: function () {

                let applications = {
                    "items": [
                        {
                            "src": "sap-icon://sap-logo-shape",
                            "title": "SAP Homepage",
                            "subTitle": "Learn more about SAP",
                            "targetSrc": "https://www.sap.com/index.html",
                            "target": "_blank"
                        },
                        {
                            "src": "sap-icon://group",
                            "title": "Community",
                            "subTitle": "Get involved",
                            "targetSrc": "https://community.sap.com/topics/ui5",
                            "target": "_blank"
                        }
                    ]
                }

                let oView = this.getView()

                let oModel = new JSONModel(applications)

                oView.setModel(oModel, "applicationsModel")

            },

            _initApplicationPopover: function () {

                let oView = this.getView()

                if (!this._pPopover) {
                    this._pPopover = Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hana.mantparam.view.fragment.ApplicationSwitch",
                        controller: this
                    }).then(function (oPopover) {
                        oView.addDependent(oPopover);
                        if (Device.system.phone) {
                            oPopover.setEndButton(new Button({ text: "Close", type: "Emphasized", press: this.fnClose.bind(this) }));
                        }
                        return oPopover;
                    }.bind(this));
                }

            },

            loadCreateFormPopover: function() {
                var oView = this.getView();
                let oContext = this;
                
                if (!this._oFormPopover) {
                    var oCloseButton =  new Button({
                        text: "Cerrar",
                        press: function () {
                            oContext._oFormPopover.close();
                        }
                    }).addStyleClass("sapUiTinyMarginEnd"),
                    oPopoverFooter = new Bar({
                        contentRight: oCloseButton
                    }),
                    oPopoverBar = new Bar({
                        contentMiddle: [
                            new Title({text: "Global Setting Creation/Update"})
                        ]
                    });
    
                    this._oFormPopover = new Popover({
                        customHeader: oPopoverBar,
                        contentWidth: "auto",
                        verticalScrolling: false,
                        placement: sap.m.PlacementType.Left,
                        modal: true,
                        footer: oPopoverFooter
                    })
    
                    Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hana.mantparam.view.fragment.FormApp",
                        controller: this
                    }).then(function(oFragment) {
                        oView.addDependent(oFragment);
                        this._oFormPopover.addContent(oFragment);
                        this._oFormPopover.openBy(oView.byId("_IDGenButton1"));
                    }.bind(this));
                } else {
                    this._oFormPopover.openBy(oView.byId("_IDGenButton1"));
                }
            },
            
            loadCreateForm: function() {
                var oView = this.getView();
                let oContext = this;
                let newModel = new JSONModel()
                if (!newModel.getProperty("/GlobalSettingClassification")) {
                    newModel.setProperty("/GlobalSettingClassification", "PATTERN");
                }
                if (!newModel.getProperty("/GlobalSettingStatus")) {
                    newModel.setProperty("/GlobalSettingStatus", "ACTIVO");
                }
                oContext.getView().setModel(newModel, "newGlobalSetting")
                if (!this._oFormDialog) {
                    var oDialogBar = new Bar({
                        contentMiddle: [
                            new Title({text: "Global Setting Creation/Update"})
                        ]
                    });
    
                    this._oFormDialog = new Dialog({
                        customHeader: oDialogBar,
                        contentWidth: "40%",
                        verticalScrolling: false,
                    })
    
                    Fragment.load({
                        id: oView.getId(),
                        name: "atriacorps4hana.mantparam.view.fragment.FormApp",
                        controller: this
                    }).then(function(oFragment) {
                        /*console.log("TestFrag1",oFragment)
                        console.log("TestFrag2",oFragment.getMetadata())
                        console.log("TestFrag3",oFragment.getContent())
                        
                        //oFragment.getContent().byId('globalSettingStatusSwitch').setStyleClass('largerSwitch')
                        var aSwitches = oFragment.getContent().filter(function(oItem) {
                            return oItem instanceof sap.m.Switch;
                        });

                        aSwitches.forEach(function(aSwitch) {
                            console.log(aSwitch);
                            console.log(aSwitch.getMetadata())
                            aSwitch.addStyleClass('largerSwitch');
                        });*/
                        oView.addDependent(oFragment);
                        this._oFormDialog.addContent(oFragment);
                        this._oFormDialog.open();
                    }.bind(this));
                } else {
                    oContext._oFormDialog.open();
                }
            },

            saveGSonERP:async function (oEvent) {
                debugger
                this._initBusyIndicator();
                var requestBody = this.getView().getModel("newGlobalSetting").getData()
                 
                //var endpoint = "/s4hana-extensibility/odata.svc/GlobalSettings";
                var endpoint = `${sDomain+sDestination}odata.svc/GlobalSettings`;
                
                var requestOptions = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(requestBody)
                };

                try {
                    let request = await fetch(endpoint, requestOptions);
                    if (request.status === 200 || request.status === 201) {
                        MessageToast.show("Operación exitosa. El parámetro fue creado correctamente.");
                        this.refreshoData()
                    } else {
                        MessageBox.error("Ocurrió un problema al crear el parámetro. Por favor, inténtelo nuevamente.");
                    }
                } catch (error) {
                    MessageBox.error("Ocurrió un problema al comunicarse con el servidor. Por favor, inténtelo nuevamente.");
                } finally {
                    this._hideBusyIndicator();
                    if (this._oFormDialog) {
                        this._oFormDialog.close();
                    }
                }
                
                

            },

            cancelCreate: function (oEvent) {
                this._oFormDialog.close();

            },

            fnChange: function (oEvent) {
                var oItemPressed = oEvent.getParameter("itemPressed"),
                    sTargetSrc = oItemPressed.getTargetSrc();

                MessageToast.show("Redireccionando... " + sTargetSrc);

                URLHelper.redirect(sTargetSrc, true);
            },
            fnOpen: function (oEvent) {
                var oButton = this.getView().byId("pSwitchBtn");
                this._pPopover.then(function (oPopover) {
                    oPopover.openBy(oButton);
                });
            },
            fnClose: function () {
                this._pPopover.then(function (oPopover) {
                    oPopover.close();
                });
            }

        });
    });
