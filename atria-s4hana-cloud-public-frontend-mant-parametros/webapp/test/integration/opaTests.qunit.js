/* global QUnit */

sap.ui.require(["atriacorps4hana/mantparam/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
