sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function ( Controller, MessageBox ) {
	"use strict";

var sResponsivePaddingClasses = "sapUiResponsivePadding--header sapUiResponsivePadding--content sapUiResponsivePadding--footer";

	return Controller.extend("atriacorps4hana.logeventosnma.controller.App", {
		
		getNamespace: function(){
			return "com.csti.nma.moduloaprobacioneslogeventos.bnvlogeventosnma."; 
		},
		onInit: function () {
			this.dialog = null;
            this._defineCustomModel()
		},
		onInitialise: function (oEvent) {
			var that = this;
			var oSmartTable = this.getView().byId("IdViewLogModuloAprobacion");
			var oContext;
			oSmartTable._oTable.setEnableColumnReordering(true);
			oSmartTable._oTable.setEnableColumnFreeze(true);
			oSmartTable._oTable.setFixedColumnCount(1);
			oSmartTable._oTable.getColumns().forEach(function (oColumn, iColumn) {
				oContext = that._createTableLog(oColumn.getFilterProperty());
				oColumn.setHAlign(oContext.hAlign);
				oColumn.setWidth(oContext.width);
				oColumn.getLabel().setTextAlign("Center");
				oColumn.getLabel().setWrapping(true);
				oColumn.getLabel().setWrappingType("Hyphenated");
				/*oColumn.mAggregations.customData[0].mProperties.value.type = oContext.type;
				oColumn.mAggregations.customData[0].mProperties.value.width = null;*/
				oColumn.setTemplate(oContext.control);
			});
		},
		onBeforeRebind: function(oEvent){
			var aBindingParams = oEvent.getParameter("bindingParams");
			aBindingParams.filters = $.map(aBindingParams.filters, function (oItem) {
				
				if(["FechaTransaccion"].includes(oItem.sPath)){
					oItem.sPath = oItem.sPath + "Filtro";
				}
	
				return oItem;
			});	
		},
		_createTableLog: function (sIndex) {
			var that = this;
			var aColumns = [
			{
				hAlign: "Center",
				width: "5rem",
				name: "Id",
				control: new sap.m.Text({
					text: "{Id}",
					wrapping: false
				}),
				type: "number"
			}, {
				hAlign: "Center",
				width: "15rem",
				name: "Param1",
				control: new sap.m.Text({
					text: "{Param1}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}, {
				hAlign: "Center",
				width: "15rem",
				name: "Param2",
				control: new sap.m.Text({
					text: "{Param2}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}, {
				hAlign: "Center",
				width: "9rem",
				name: "Param3",
				control: new sap.m.Text({
					text: "{Param3}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}, {
				hAlign: "Center",
				width: "15rem",
				name: "Param4",
				control: new sap.m.Text({
					text: "{Param4}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}, {
				hAlign: "Center",
				width: "12rem",
				name: "Param5",
				control: new sap.m.Text({
					text: "{Param5}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}, {
				hAlign: "Center",
				width: "12rem",
				name: "SendDatetime",
				control: new sap.m.Text({
					text: "{ path: 'SendDatetime', type: 'sap.ui.model.type.DateTime', formatOptions: {UTC: true, pattern: 'dd.MM.yyyy'} }",
					wrapping: false,
					maxLines: 1
				}),
				type: "Date"
			},{
				hAlign: "Center",
				width: "12rem",
				name: "State",
				control: new sap.m.Text({
					text: "{State}",
					wrapping: false,
					maxLines: 1
				}),
				type: "string"
			}
			];
			var aColumn = $.grep(aColumns, function (oColumn) {
				if (sIndex == oColumn.name)
					return oColumn;
			});

			return aColumn.length > 0 ? aColumn[0] : {};
		},
        _defineCustomModel: function () {

            debugger

            let oView = this.getView()

            let oTable = oView.byId("IdViewLogModuloAprobacion")
           //-- let oFilter = oView.byId("smartFilterBar")
            let oDataModel = this.getOwnerComponent().getModel("odata")

            oTable.setModel(oDataModel)
            //oView.byId("LineItemsSmartTable").setModel(oDataModel)
            //oFilter.setModel(oDataModel)

        },
		_onPreviewDifference: function( oEvent ){
			
			//--- Obtenemos el ID del registro
			var entity = oEvent.getSource().getBindingContext().getObject();
			var idLog = entity.Id;
			
			var url = "/licycon/api-rest-/audit/log-modulo-aprobaciones/find-diference/" + idLog;
			var _this = this;
			
			//--- Leemos los datos de las diferencias
			this.onGet( url, function( data ) {
				
				_this.getFragment( _this.getNamespace() + "view.fragment.ViewDifferenceDialog" ).open();
				sap.ui.getCore().byId("IdCodeEditor").setValue( JSON.stringify( data, null, "\t" ) );
			
			}, 
			function( error ) {
				_this.onShowJSONInfo( error );
			}, 
			function( complete ) {
				
			} );
			
		},
		closeFragmentDiff: function(  ){
			this.closeFragment( this.getNamespace() + "view.fragment.ViewDifferenceDialog" );	
		},
		onShowJSONInfo: function ( error ) {
			
			debugger;
			
			MessageBox.error("Detalle del problema", {
				title: "Ocurrió un problema",
				id: "messageBoxId1",
				details: error.responseJSON,
				contentWidth: "300px",
				styleClass: sResponsivePaddingClasses
			});
			
		},
		_onRebindTable:function(){
			this.getView().byId("IdViewLogModuloAprobacion").rebindTable();
		},
		//--------------------------------------------------------------
		//Fragment
		//--------------------------------------------------------------
		getFragment : function(fragment){
			
			if(!this.dialog){
				this.dialog = sap.ui.xmlfragment(fragment, this);
				this.getView().addDependent(this.dialog);
			}
			return this.dialog;
		},
		closeFragment: function(fragment){
			try{
				this.dialog.close();
			}catch(ex){
				console.log("Dialogo no encontrado ...");
			}
		},
		resetFragment: function(){
			this.dialog = null;
		},
		
		//-------------------------------------------------------------------------------------------------------------
		// Ajax
		//-------------------------------------------------------------------------------------------------------------
		onGet : function(endpoint, fSuccess, fErrors, fComplete){
			$.ajax({
				url			: endpoint				,
				type		: "GET"					,
				async		: true					,
				headers		: {
					"Accept": "application/json"
				},
				success		: function(response){fSuccess(response)}	,
				error		: function(errors){fErrors(errors)}			,
				complete	: function(){fComplete()}
			});
		},
	});
});




