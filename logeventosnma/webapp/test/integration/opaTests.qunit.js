/* global QUnit */

sap.ui.require(["atriacorps4hana/logeventosnma/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
