/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"atriacorps4hanape/mantbp/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
