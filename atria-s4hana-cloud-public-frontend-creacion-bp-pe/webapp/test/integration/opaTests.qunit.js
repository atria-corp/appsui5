/* global QUnit */

sap.ui.require(["atriacorps4hanape/mantbp/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
